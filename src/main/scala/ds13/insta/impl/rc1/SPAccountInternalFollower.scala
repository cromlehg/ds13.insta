package ds13.insta.impl.rc1

import scala.collection.mutable.ListBuffer

import ds13.bots.StateMachineBotProcess
import ds13.logger.Logger
import ds13.logger.LoggerSupported
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import org.json.JSONObject

class SPAccountInternalFollower(
  override val id: Long,
  override val logger: Logger,
  userName: String,
  accountName: String,
  usernameToGetFollowings: String)
    extends StateMachineBotProcess[StateMachineBotResult, StateMachineBotConfig] with LoggerSupported {

  val STATE_FINISHED = 0

  val STATE_NEEDS_FOLLOW = STATE_FINISHED + 1

  state = STATE_NEEDS_FOLLOW

  val api = new APIExecutor(logger, userName, accountName)

  val provider = new UserFollowingsUsersProvider(api, usernameToGetFollowings)

  var preparedUsersCount = 0

  override def toJSON: Option[JSONObject] = None

  override def step =
    state match {
      case STATE_NEEDS_FOLLOW =>
        provider.fold {
          debug("Provider has no more users!")
          debug("Summay followed: " + preparedUsersCount)
          setState(SPStates.STATE_FINISHED)
          isFinishedFlag = true
        } { user =>
          api.executeInternal { internalAPI =>
            debug("Try to follow user with Id: " + user.toString)
            internalAPI.follow(user.id)
            preparedUsersCount += 1
            debug("User: " + user.toString + " - followed successfully")
            debug("Summay followed: " + preparedUsersCount)
          }
        }
      case _ =>
        err("Unknwon state occurs!")
        setState(SPStates.STATE_FINISHED)
        isFinishedFlag = true
    }

  override def getPause =
    state match {
      case STATE_NEEDS_FOLLOW => 90000
      case _                  => 1000
    }
}

