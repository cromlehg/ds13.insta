package ds13.insta.impl.rc1

import scala.collection.mutable.ListBuffer

import ds13.bots.StateMachineBotProcess
import ds13.logger.Logger
import ds13.logger.LoggerSupported
import org.json.JSONObject

class StateMachineBotResult

class StateMachineBotConfig

class SPAccountInternalUnfollower(
  override val id: Long,
  override val logger: Logger,
  userName: String,
  accountName: String)
    extends StateMachineBotProcess[StateMachineBotResult, StateMachineBotConfig] with LoggerSupported {

  state = SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL

  val api = new APIExecutor(logger, userName, accountName)

  var nextFollwoingsMaxId: Option[String] = None

  val usersPool = ListBuffer[Long]()

  val preparedUsers = ListBuffer[Long]()

  override def toJSON: Option[JSONObject] = None

  override def step =
    state match {
      case SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL =>
        api.executeInternal { internalAPI =>
          val r = internalAPI.getSelfFollowings(nextFollwoingsMaxId)
          nextFollwoingsMaxId = r.nextMaxIdOpt
          usersPool ++= r.users.map(_.id)
          debug("Unfollow users pool filled, now it have size: " + usersPool.length)
          setState(SPStates.STATE_NEEDS_UNFOLLOW)
        }
      case SPStates.STATE_NEEDS_UNFOLLOW =>
        if (usersPool.isEmpty) {
          nextFollwoingsMaxId match {
            case Some(nextMaxId) =>
              debug("Followings pool is empty. Going to pool fill")
              setState(SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL)
            case _ =>
              debug("Unfollowing process finished!")
              setState(SPStates.STATE_FINISHED)
              isFinishedFlag = true
          }
        } else
          api.executeInternal { internalAPI =>
            val userToUnfollow = usersPool.head
            debug("Try to unfollow user with Id: " + userToUnfollow)
            internalAPI.unfollow(userToUnfollow)
            usersPool -= userToUnfollow
            preparedUsers += userToUnfollow
            debug("User with id = " + userToUnfollow + " unfollowed successfully")
            debug("Summay unfollowed: " + preparedUsers.length)
          }
      case _ =>
    }

  override def getPause =
    state match {
      case SPStates.STATE_NEEDS_FOLLOWINGS_POOL_FILL => 1000
      case SPStates.STATE_NEEDS_UNFOLLOW             => 70000
      case SPStates.STATE_FINISHED                   => 1000
      case _                                         => 60000
    }

}

object SPStates {

  val STATE_UNKNOWN = -1

  val STATE_FINISHED = STATE_UNKNOWN + 1

  val STATE_NEEDS_FOLLOWINGS_POOL_FILL = STATE_FINISHED + 1

  val STATE_NEEDS_UNFOLLOW = STATE_NEEDS_FOLLOWINGS_POOL_FILL + 1

  val STATE_NEEDS_GET_USER_FROM_PROVIDER = STATE_NEEDS_UNFOLLOW + 1

  val STATE_NEEDS_FOLLOW = STATE_NEEDS_GET_USER_FROM_PROVIDER + 1

}
