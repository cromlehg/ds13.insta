package ds13.insta.impl.rc1

import ds13.insta.api.intrnl.InternalAPI
import ds13.insta.api.intrnl.InternalAPI5Consts
import ds13.insta.api.web.WebAPI
import ds13.insta.api.web.WebAPIConsts
import ds13.insta.storage.TraitAPIStorage
import ds13.insta.storage.TraitAccountStorage
import ds13.insta.storage.TraitLaunchStorage
import ds13.insta.storage.TraitStorage
import ds13.insta.storage.TraitUserStorage
import ds13.insta.storage.slick.mysql.SlickMySQLStorage
import ds13.logger.Logger
import ds13.logger.LoggerSupported

class APIExecutor(override val logger: Logger, username: String, accountname: String) extends LoggerSupported {

  var storage: Option[TraitStorage] = None

  var internalAPI: Option[InternalAPI] = None

  var internalAPILaunchStorage: Option[TraitLaunchStorage] = None

  var webAPI: Option[WebAPI] = None

  var launchStorage: Option[TraitLaunchStorage] = None

  def getStorage: Option[TraitStorage] = {
    if (storage.isEmpty)
      storage = Some(new SlickMySQLStorage(logger))
    storage
  }

  def getUserStorage: Option[TraitUserStorage] =
    getStorage match {
      case Some(storage) =>
        debug("Try to get user storage")
        storage.getUserStorageByLogin(username) match {
          case Some(userStorage) =>
            Some(userStorage)
          case _ =>
            err("Can't get user storage with user name: " + username)
            None
        }
      case _ =>
        err("Can't get user storage because storage is not defined!")
        None
    }

  def getAccountStorage: Option[TraitAccountStorage] =
    getUserStorage match {
      case Some(userStorage) =>
        debug("Try to get account storage")
        userStorage.getAccountStorageByLogin(accountname) match {
          case Some(accountStorage) =>
            Some(accountStorage)
          case _ =>
            err("Can't get account storage with account name: " + accountname)
            None
        }
      case _ =>
        err("Can't get account storage because user storage is not defined!")
        None
    }

  def getWebAPIStorage: Option[TraitAPIStorage] =
    getAccountStorage match {
      case Some(accountStorage) =>
        debug("Try to get web API storage")
        accountStorage.getAPIStorageByNameAndVersion(WebAPIConsts.NAME, WebAPIConsts.VERSION) match {
          case Some(apiStorage) =>
            Some(apiStorage)
          case _ =>
            err("Can't get web API storage")
            None
        }
      case _ =>
        err("Can't get web API storage because account storage is not defined!")
        None
    }

  def getInternalAPIStorage: Option[TraitAPIStorage] =
    getAccountStorage match {
      case Some(accountStorage) =>
        debug("Try to get internal API storage")
        accountStorage.getAPIStorageByNameAndVersion(InternalAPI5Consts.NAME, InternalAPI5Consts.VERSION) match {
          case Some(apiStorage) =>
            Some(apiStorage)
          case _ =>
            err("Can't get internal API storage v5")
            None
        }
      case _ =>
        err("Can't get internal API storage because account storage is not defined!")
        None
    }

  def getInternalAPI: Option[InternalAPI] = {
    if (internalAPI.isEmpty)
      internalAPI = getInternalAPIStorage match {
        case Some(apiStorage) =>
          debug("Internal API storage initialized - OK!")
          debug("Try to create internal API...")
          Some(new InternalAPI(apiStorage))
        case _ =>
          err("Can't create internal API because can't get internal API storage!")
          None
      }
    internalAPI
  }

  def getWebAPI: Option[WebAPI] = {
    if (webAPI.isEmpty)
      webAPI = getWebAPIStorage match {
        case Some(apiStorage) =>
          debug("Web API initialized - OK!")
          Some(new WebAPI(apiStorage))
        case _ =>
          err("Can't create Web API because can't get web API storage!")
          None
      }
    webAPI
  }

  def getLaunchStorage(id: Long, name: String): Option[TraitLaunchStorage] = {
    if (launchStorage.isEmpty)
      launchStorage = getInternalAPIStorage match {
        case Some(apiStorage) =>
          apiStorage.getLaunchStorageByIdAndName(id, name)
        case _ =>
          err("Can't create internal API because can't get internal API storage!")
          None
      }
    launchStorage
  }

  def updateState(id: Long, name: String, state: Option[String]) =
    launchStorageFold(id, name)(err("Some problems during update state"))(_.updateExtState(state))

  def launchStorageFold[T](id: Long, name: String)(ifNot: => T)(f: TraitLaunchStorage => T) =
    getLaunchStorage(id, name).fold(ifNot)(f)

  def internalFold[T](ifNot: => T)(f: InternalAPI => T) =
    getInternalAPI.fold(ifNot)(f)

  def executeInternal(f: InternalAPI => Unit): Boolean =
    getInternalAPI match {
      case Some(api) =>
        f(api)
        true
      case _ => false
    }

  def executeWeb(f: WebAPI => Unit): Boolean =
    getWebAPI match {
      case Some(api) =>
        f(api)
        true
      case _ => false
    }

  def execute(f: (InternalAPI, WebAPI) => Unit): Boolean =
    (getInternalAPI, getWebAPI) match {
      case (Some(internalAPI), Some(webAPI)) =>
        f(internalAPI, webAPI)
        true
      case (Some(internalAPI), _) =>
        err("Some problems with getting web API!")
        false
      case (_, Some(webAPI)) =>
        err("Some problems with getting internal API!")
        false
      case (_, _) =>
        err("Some problems with getting internal and web API!")
        false
    }

  def dispose =
    storage foreach (_.dispose)

}