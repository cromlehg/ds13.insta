package ds13.insta.impl.rc1

import ds13.bots.StateMachineBotProcess

class SPExecutor(stateMachine: StateMachineBotProcess[_, _]) {

  def execute =
    while (!stateMachine.isFinished) {
      stateMachine.step
      Thread.sleep(stateMachine.pause)
    }

}