package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.SPAccountInternalFollower
import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor
import scala.collection.mutable.ListBuffer

object NotToFollowStorageTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  api.executeInternal { i =>
    i.getSelfUserInfo
  }

  api.getAccountStorage.foreach { as =>
    val notToFollowStorage = as.getNotToFollowList.get
    val notToFollow = ListBuffer[Long]()
    println(notToFollowStorage.contains(25025320))
  }

}