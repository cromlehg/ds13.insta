package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object InternalAPILoginAndGetFollowers1 extends App {

  new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg").executeInternal { client =>
    client.getFollowers(1709504388, None)
  }

}