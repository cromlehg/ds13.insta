package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object GetLocationFeedByIdTest1 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "partizan", "mihaluch")

  api.executeInternal { iapi =>
    val response = iapi.getLocationFeed(270091665, None)
    println(response)
    println(response)
  }

  api.dispose

}