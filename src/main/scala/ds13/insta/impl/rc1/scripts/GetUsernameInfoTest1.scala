package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.providers.TraitFilter

object GetUsernameInfoTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  val apiExecutor = new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg")

  val provider = new LESFICompiler().parseProvider(apiExecutor, "&615458553").get

  implicit val filter = Some(new TraitFilter[User] {

    override def skip(user: User): Boolean =
      true

  })
  
  
  val result = provider.hasNext()(filter)
  
  println(result)
  println(result)

}