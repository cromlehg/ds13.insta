package ds13.insta.impl.rc1.scripts.tests.lesfi

import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object SimpleCompTest3 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  val value = "@muradosmann"

  val providerOpt = new LESFICompiler().parseProvider(api, value)

  providerOpt.foreach(_.fold(println("empty"))(t => println("first: " + t.username)))
  providerOpt.foreach(_.fold(println("empty"))(t => println("second: " + t.username)))
  providerOpt.foreach(_.fold(println("empty"))(t => println("thrid: " + t.username)))

}
