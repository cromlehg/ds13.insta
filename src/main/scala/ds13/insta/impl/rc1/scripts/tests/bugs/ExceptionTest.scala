package ds13.insta.impl.rc1.scripts.tests.bugs

import org.apache.http.NoHttpResponseException
import org.apache.http.conn.ConnectTimeoutException
import ds13.insta.api.ServiceUnavaliableException
import ds13.insta.api.BadGatewayException
import ds13.insta.api.InternalServerErrorException
import javax.net.ssl.SSLException

class ExceptionTest {

  var vOpt: Option[Int] = None

  def infTryAfterException[T](f: => T): T =
    try f catch {
      case e @ (_: ServiceUnavaliableException |
        _: SSLException |
        _: BadGatewayException |
        _: InternalServerErrorException |
        _: ConnectTimeoutException |
        _: NoHttpResponseException) =>
        println("CATCHED")
        throw e
      case e: Throwable =>
        println("Unprepared exception...")
        throw e
    }

  def loggedIn[T](f: Int => T): T = {
    println("before")
    val r = f(1)
    println("after")
    r
  }

  private def infTryAfterExceptionLoggedIn[T](f: Int => T): T =
    // what difference between 
    //loggedIn(infTryAfterException(f))
    // and
    loggedIn(t => infTryAfterException(f(t)))

  def test =
    infTryAfterExceptionLoggedIn { t =>
      throw new BadGatewayException(null)
    }

}

object ExceptionTest extends App {

  new ExceptionTest().test

}