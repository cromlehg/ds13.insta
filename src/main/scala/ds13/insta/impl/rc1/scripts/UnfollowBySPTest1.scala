package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.SPAccountInternalUnfollower
import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger

object UnfollowBySPTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  val logger = PrintAllLogger()

  val stateMachine = new SPAccountInternalUnfollower(0, logger, "iceberg", "cromlehg")

  val executor = new SPExecutor(stateMachine)

  executor.execute

}