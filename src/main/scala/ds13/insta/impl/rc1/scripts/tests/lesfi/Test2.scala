package ds13.insta.impl.rc1.scripts.tests.lesfi

import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor

object SimpleCompTest2 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "partizan", "mihaluch")

  val value = "@muradosmann"

  val providerOpt = new LESFICompiler().parseProvider(api, value)

  providerOpt.foreach { provider =>

    println(provider.toJSON.get)

    if (provider.hasNext) {
      println("Provier has users...")
      provider.fold(println("empty"))(println)
    } else {
      println("Provier is empty - all finished!")
    }

  }

  api.dispose

}
