package ds13.insta.impl.rc1.scripts.tests.bugs

import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor

object CBBug1 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  val bot = new ComplexBot1(logger, 1, "Complex bot 1", api, "@muradosmann", false, false, 3000, true, true, 90000, true, false)

  val executor = new SPExecutor(bot).execute

}