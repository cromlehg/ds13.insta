package ds13.insta.impl.rc1.scripts.tests.bugs

import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler

object MaratIbragimov05Bug extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  val provider = new LESFICompiler().parseProvider(api, "@marat_ibragimov05").get

  var count = 0

  while (provider.hasNext) {
    count += 1
    provider.fold {
      println("Fininshed")
    } { user =>
      println(user.id + " = " + user.username + " - " + count)
    }
  }

  api.dispose

}