package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object LikeMediaTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg").executeInternal { client =>
    if (client.like(1347869791259748878L)) {
      println("Like is OK!")
    } else {
      println("Some problems with like!")
    }
  }

}