package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object WebAPILoginTest1 extends App {

  new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg").executeWeb { client =>
    if (client.login) {
      client.info("Login - OK!")
    } else {
      client.info("Login - Failed!")
    }
  }

}