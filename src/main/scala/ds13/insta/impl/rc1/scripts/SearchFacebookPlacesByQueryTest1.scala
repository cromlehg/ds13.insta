package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object SearchFacebookPlacesByQueryTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "partizan", "mihaluch")

  api.executeInternal { iapi =>
    val result = iapi.searchFacebookLocation("кусково", None)
    println(result)
    println(result)
  }

  api.dispose

}