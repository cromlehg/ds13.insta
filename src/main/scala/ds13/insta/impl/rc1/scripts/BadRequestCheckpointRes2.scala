package ds13.insta.impl.rc1.scripts

import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor

object BadRequestCheckpointRes2 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "partizan", "mihaluch")

  api.executeInternal { i =>
    i.getSelfFollowers(None)
  }

}