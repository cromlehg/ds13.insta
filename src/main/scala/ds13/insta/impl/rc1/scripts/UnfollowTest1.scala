package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object UnfollowTest1 extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg").executeInternal { client =>
    val result = client.unfollow(1728626106)
    println(result)
    println(result)
  }

}