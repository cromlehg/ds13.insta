package ds13.insta.impl.rc1.scripts.tests.lesfi

import ds13.insta.api.intrnl.model.lesfi.LESFICompiler

object SimpleCompTest extends App {

  val value = "#ss #ssd @fffj"
  println(new LESFICompiler().checks(value))
  println(new LESFICompiler().parseProvider(null, value))

}
