package ds13.insta.impl.rc1.scripts

import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor

object NoAthorizedToViewUserBug1 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  api.executeInternal { i =>
    val info = i.getUserInfo(2108046004)
    println(info)
    println(info)
    val feeds = i.getUserFeed(2108046004)
    println(feeds)
    println(feeds)
  }

}