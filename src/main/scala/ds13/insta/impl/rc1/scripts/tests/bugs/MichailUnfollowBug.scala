package ds13.insta.impl.rc1.scripts.tests.bugs

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object MicahelUnfollowBug extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "partizan", "mihaluch")

  api.executeInternal { iapi =>
    iapi.unfollow(1239885747)
  }

  api.dispose

}