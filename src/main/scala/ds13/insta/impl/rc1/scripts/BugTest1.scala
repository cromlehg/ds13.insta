package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.SPAccountInternalFollower
import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor
import scala.io.Source

object FillTest1 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "apetchaninov", "Petchaninov")

  val path = "/home/iceberg/projects/ds4/insta1/anton/state3/"

  val nonUnfollowPath = path + "nonunfollow"

  val nonFollowPath = path + "nonfollow"

  def readFromFile(p: String) = Source.fromFile(p).getLines.toSeq.map(_.trim).filterNot(_.isEmpty).distinct.map(_.toLong)

  val nonUnfollowList = readFromFile(nonUnfollowPath)

  val nonFollowList = (readFromFile(nonFollowPath) ++ nonUnfollowList).distinct

  val everFollowedList = (nonUnfollowList ++ nonFollowList).distinct

  println("Non unfollow: " + nonUnfollowList.length)
  println("Non follow: " + nonFollowList.length)
  println("Ever followed: " + everFollowedList.length)

  api.getAccountStorage.foreach { storage =>
    println(storage.addNotToUnFollowList(nonUnfollowList))
    println("1")
    println(storage.addEverFollowedList(everFollowedList))
    println("2")
    println(storage.addNotToFollowList(nonFollowList))
    println("3")
    println("Ok")
  }

  api.dispose

}