package ds13.insta.impl.rc1.scripts.tests.bugs

import ds13.insta.impl.rc1.SPExecutor
import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler

object MaratIbragimov05Bug2 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  val provider = new LESFICompiler().parseProvider(api, "@marat_ibragimov05").get

  var count = 0

  api.executeInternal { iapi =>
    val res1 = iapi.getFollowers(1901218716, None)
    val res2 = iapi.getFollowers(1901218716, res1.nextMaxIdOpt)
    println(res1)
    println(res2)
    println(res1)
    println(res2)

  }

  //  while (provider.hasNext) {
  //    count += 1
  //    provider.fold {
  //      println("Fininshed")
  //    } { user =>
  //      println(user.id + " = " + user.username + " - " + count)
  //    }
  //  }

  api.dispose

}