package ds13.insta.impl.rc1.scripts

import ds13.logger.PrintAllLogger
import ds13.insta.impl.rc1.APIExecutor

object BadRequestCheckpointRes extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "apetchaninov", "Petchaninov")

  api.executeInternal { i =>
    val followers = i.getSelfFollowers(None)
    println(followers)
    println(followers)
  }

}