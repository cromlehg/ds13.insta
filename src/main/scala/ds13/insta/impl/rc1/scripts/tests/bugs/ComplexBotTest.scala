package ds13.insta.impl.rc1.scripts.tests.bugs

import ds13.insta.impl.rc1.SPStates
import ds13.insta.api.intrnl.model.lesfi.LESFICompiler
import ds13.insta.api.intrnl.model.UsersList
import ds13.insta.storage.TraitNotToUnFollowListStorage
import ds13.insta.impl.rc1.APIExecutor
import scala.collection.mutable.ListBuffer
import ds13.insta.storage.TraitNotToFollowListStorage
import ds13.insta.api.intrnl.model.User
import org.json.JSONObject

object ComplexBot1 {

  val STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow = SPStates.STATE_NEEDS_FOLLOW + 1

  val STATE_fillSelfFollowersAsNonToFollow = STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow + 1

  val STATE_follow = STATE_fillSelfFollowersAsNonToFollow + 1

  val STATE_unfollow = STATE_follow + 1

  val STATE_MAIN_LOOP_PREPARE = STATE_unfollow + 1

  val STATE_Wait_loop = STATE_MAIN_LOOP_PREPARE + 1

  val STATE_SMALL_PAUSE_follow = STATE_Wait_loop + 1

  val STATE_SMALL_PAUSE_unfollow = STATE_SMALL_PAUSE_follow + 1

  val STATE_NEEDS_PREPARE = STATE_SMALL_PAUSE_unfollow + 1

}

class ComplexBot1(
  logger: ds13.logger.Logger,
  id: Long,
  name: String,
  api: APIExecutor,
  lesfi: String,
  isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow: Boolean,
  isNeedToFillSelfFollowersAsNonToFollow: Boolean,
  followLimit: Int,
  isFirstFollow: Boolean,
  isLoop: Boolean,
  timeBetweenLoop: Long,
  isLikeLastPostBeforeFollow: Boolean,
  isFollowToPrivate: Boolean)
    extends IBEFEBotProcess(logger, id, api, name) {

  state = ComplexBot1.STATE_NEEDS_PREPARE

  val provider = new LESFICompiler().parseProvider(api, lesfi).get

  var summaryUsersFollowedCount = 0

  var summaryUsersUnfollowedCount = 0

  var userId: Long = -1

  var nextIdFollowers: Option[String] = None

  var nextIdFollowings: Option[String] = None

  var followedStageCount = 0

  var prevState = SPStates.STATE_UNKNOWN

  var notToFollowStorage: TraitNotToFollowListStorage = null

  var notToUnfollowStorage: TraitNotToUnFollowListStorage = null

  var notToFollow = ListBuffer[Long]()

  var unfollowListOpt: Option[UsersList] = None

  var unfollowListIndex = 0

  var toUnfollow = ListBuffer[User]()

  override def catchedStep: Unit =
    state match {
      case ComplexBot1.STATE_NEEDS_PREPARE =>
        debug("Prepare in progress...")
        if (!api.executeInternal { internalAPI =>
          if (isNeedToFillSelfFollowingsAsNonToUnfollowAndNonToFollow) {
            setState(ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow)
          } else if (isNeedToFillSelfFollowersAsNonToFollow) {
            setState(ComplexBot1.STATE_fillSelfFollowersAsNonToFollow)
          } else {
            setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)
          }
          debug("State has been changed after prepare to " + state)
        }) errFinish("Some internal problems with API initialization!")
      case ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow =>
        debug("Fill self followers state...")
        api.executeInternal { iapi =>
          val usersList = iapi.getSelfFollowings(nextIdFollowings)
          api.getAccountStorage.foreach { as =>
            val ids = usersList.users.map(_.id)
            debug("Prepare followers size " + ids.length)
            as.addEverFollowedList(ids)
            as.addNotToFollowList(ids)
            if (isNeedToFillSelfFollowersAsNonToFollow) as.addNotToUnFollowList(ids)
            usersList.nextMaxIdOpt.fold(
              if (isNeedToFillSelfFollowersAsNonToFollow) {
                setState(ComplexBot1.STATE_fillSelfFollowersAsNonToFollow)
              } else {
                setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE)
              })(t => nextIdFollowings = Some(t))
          }
        }
      case ComplexBot1.STATE_fillSelfFollowersAsNonToFollow =>
        debug("Fill self followings state...")
        api.executeInternal { iapi =>
          val usersList = iapi.getSelfFollowers(nextIdFollowers)
          api.getAccountStorage.foreach { as =>
            val ids = usersList.users.map(_.id)
            debug("Prepare followings size " + ids.length)
            as.addNotToFollowList(ids)
            usersList.nextMaxIdOpt.fold(setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE))(t => nextIdFollowers = Some(t))
          }
        }
      case ComplexBot1.STATE_MAIN_LOOP_PREPARE =>
        debug("Main loop router started...")
        var isFirst = false
        if (prevState == SPStates.STATE_UNKNOWN) {
          isFirst = true
          debug("Previous loop state is unknown.")
          prevState = if (isFirstFollow) ComplexBot1.STATE_unfollow else ComplexBot1.STATE_follow
          debug("Set prev state to: " + prevState + " - " + (if (isFirstFollow) "unfollow" else "follow") + ".")
        }
        prevState match {
          case ComplexBot1.STATE_unfollow =>
            debug("Previous loop state unknown or unfollow")
            debug("Prepare to follow loop....")
            if (provider.hasNext)
              api.getAccountStorage.foreach { as =>
                notToFollowStorage = as.getNotToFollowList.get
                notToFollow = ListBuffer[Long]()
                followedStageCount = 0
                setState(if (isFirst) ComplexBot1.STATE_follow else ComplexBot1.STATE_Wait_loop)
                prevState = ComplexBot1.STATE_follow
              }
            else okFinish("Provider empty - all finnished!")
          case ComplexBot1.STATE_follow =>
            debug("Previous loop state unknown or follow")
            debug("Prepare to unfollow loop....")
            api.getAccountStorage.foreach { as =>
              notToUnfollowStorage = as.getNotToUnFollowList.get
              setState(if (isFirst) ComplexBot1.STATE_unfollow else ComplexBot1.STATE_Wait_loop)
              prevState = ComplexBot1.STATE_unfollow
              updateUnfollowList(None)
            }
          case _ =>
            errFinish("Uknown loop state: " + prevState)
        }
      case ComplexBot1.STATE_Wait_loop =>
        debug("Waiting between loop stages finnished")
        setState(prevState)
      case ComplexBot1.STATE_follow | ComplexBot1.STATE_SMALL_PAUSE_follow =>
        debug("Follow state works now.")
        if (followedStageCount < followLimit) {
          provider.fold {
            debug("Provider has no more users...")
            isLoopFinish
          } { user =>
            debug("User taken from provider: " + user.username + " with id " + user.id)
            if (notToFollow.contains(user.id) || notToFollowStorage.contains(user.id)) {
              debug("User can't be followed because in not followed list: " + user.username + " with id " + user.id)
              setState(ComplexBot1.STATE_SMALL_PAUSE_follow)
            } else api.executeInternal { internalAPI =>
              if (isLikeLastPostBeforeFollow) {
                debug("It needs to like one post before follow... Try to get feed for " + user.id)
                if (user.isPrivate)
                  debug("Can't get feeds from private user. In this case I can't like his posts")
                else {
                  val feed = internalAPI.getUserFeed(user.id)
                  debug("User have last " + feed.items.length + " feed items")
                  if (feed.items.length > 0) {
                    debug("Try to like post with id " + feed.items.last.pk)
                    internalAPI.like(feed.items.last.pk)
                  }
                }
              }
              debug("Try to follow user: " + user.toString)
              val info = internalAPI.follow(user.id)
              api.getAccountStorage.foreach { as =>
                as.addEverFollowedList(user.id)
                as.addNotToFollowList(user.id)
                notToFollow += user.id
                followedStageCount += 1
                summaryUsersFollowedCount += 1
                toUnfollow += user
                debug("User: " + user.username + " with id " + user.id + "  - followed successfully")
                printStat
                setState(ComplexBot1.STATE_follow)
              }
            }
          }
        } else {
          debug("Follow limit per stage " + followedStageCount + " reached.")
          isLoopFinish
        }
      case ComplexBot1.STATE_unfollow | ComplexBot1.STATE_SMALL_PAUSE_unfollow =>
        debug("Unfollow state works now.")
        if (toUnfollow.isEmpty) {
          unfollowListOpt.fold(errFinish("Somthig went wrong, because unfollow list opt is null!")) { unfollowList =>
            while (if (unfollowListIndex >= unfollowList.users.length) {
              unfollowList.nextMaxIdOpt.fold {
                isLoopFinish
                false
              } { nextMaxId =>
                updateUnfollowList(Some(nextMaxId))
                unfollowListOpt match {
                  case Some(unfollowList) =>
                    if (unfollowList.users.length > 0) {
                      unfollowListOpt.foreach(unfollowUserFromUserList)
                      false
                    } else true
                  case _ => false
                }
              }
            } else {
              unfollowUserFromUserList(unfollowList)
              false
            }) {}
          }
        } else {
          val user = toUnfollow.head
          unfollowUser(user)
          toUnfollow -= user
        }
      case _ =>
        errFinish("Unknwon state occurs!")
    }

  def unfollowUserFromUserList(unfollowList: UsersList) = {
    unfollowUser(unfollowList.users(unfollowListIndex))
    unfollowListIndex += 1
  }

  def unfollowUser(user: User) = {
    if (notToUnfollowStorage.contains(user.id)) {
      debug("User can't be unfollowed because in not unfollowed list: " + user.username + " with id " + user.id)
      setState(ComplexBot1.STATE_SMALL_PAUSE_unfollow)
    } else api.executeInternal { internalAPI =>
      debug("Try to unfollow user: " + user.toString)
      internalAPI.unfollow(user.id)
      api.getAccountStorage.foreach { as =>
        summaryUsersUnfollowedCount += 1
        debug("User: " + user.username + " with id " + user.id + "  - unfollowed successfully")
        printStat
        setState(ComplexBot1.STATE_unfollow)
      }
    }
  }

  def printStat: Unit = {
    debug("Summay   followed: " + summaryUsersFollowedCount)
    debug("Summay unfollowed: " + summaryUsersUnfollowedCount)
  }

  def updateUnfollowList(nextMaxIdOpt: Option[String]) =
    api.executeInternal { iapi =>
      unfollowListOpt = Some(iapi.getSelfFollowings(nextMaxIdOpt))
      unfollowListIndex = 0
    }

  def isLoopFinish =
    if (isLoop) setState(ComplexBot1.STATE_MAIN_LOOP_PREPARE) else {
      printStat
      okFinish("Finished!")
    }

  //  @tailrec
  //  final def catchedRec(f: => Unit): Unit = 
  //    try f catch {
  //      case e: BadGatewayException =>
  //        err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
  //        e.dump(logger)
  //        debug("Try again after 60000ms")
  //        Thread.sleep(60000)
  //        catchedRec(f)
  //      case e: Throwable => throw e
  //    }
  //  
  //  def catched(f: => Unit): Unit = 
  //    try f catch {
  //      case e: BadGatewayException => 
  //        err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
  //        e.dump(logger)
  //        debug("Try again after 60000ms")
  //      case e: Throwable => throw e
  //    }

  override def getPause =
    state match {
      case ComplexBot1.STATE_fillSelfFollowingsAsNonToUnfollowAndNonToFollow => 1000
      case ComplexBot1.STATE_fillSelfFollowersAsNonToFollow => 1000
      case ComplexBot1.STATE_MAIN_LOOP_PREPARE => 1000
      case ComplexBot1.STATE_SMALL_PAUSE_follow => 1000
      case ComplexBot1.STATE_unfollow => 40000
      case ComplexBot1.STATE_follow => 90000
      case ComplexBot1.STATE_SMALL_PAUSE_unfollow => 1000
      case ComplexBot1.STATE_Wait_loop => timeBetweenLoop
      case SPStates.STATE_NEEDS_FOLLOW => 90000
      case _ => 1000
    }

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("provider", provider.toJSON.get)
    })

}

