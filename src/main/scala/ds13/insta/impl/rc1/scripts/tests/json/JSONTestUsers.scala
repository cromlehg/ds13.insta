package ds13.insta.impl.rc1.scripts.tests.json

import ds13.insta.api.intrnl.model.User
import ds13.logger.PrintAllLogger
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import ds13.insta.impl.rc1.APIExecutor
import org.json.JSONObject

object JSONTestUsers extends App {

  java.util.logging.Logger.getLogger("org.apache.http.headers").setLevel(java.util.logging.Level.FINEST)
  System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog")
  System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug")

  val log = PrintAllLogger()

  val api = new APIExecutor(log, "iceberg", "cromlehg")

  api.executeInternal { client =>
    client.info("Login - OK!")
    val provider = new UserFollowingsUsersProvider(api, "moscow")
    provider.fold(println("Empty")) { _ =>
      provider.toJSONString foreach { storedProvider =>
        val jsonObject = new JSONObject(storedProvider)
        val restoredProvider = client.parser.parseUsersProvider(api, jsonObject)
        provider.toJSONString foreach { t1 =>
          println(t1)
          restoredProvider.toJSONString foreach { t2 =>
            println(t2)
            println(t1.equals(t2))
          }
        }
      }
    }
  }
  println("success")

}