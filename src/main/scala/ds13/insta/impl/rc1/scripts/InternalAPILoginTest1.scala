package ds13.insta.impl.rc1.scripts

import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.PrintAllLogger

object InternalAPILoginTest1 extends App {

  new APIExecutor(new PrintAllLogger, "iceberg", "cromlehg").executeInternal { client =>
    client.info("Login - OK!")
  }

}