package ds13.insta.impl.rc1

import ds13.logger.Logger
import ds13.logger.LoggerSupported
import scala.collection.mutable.ListBuffer

class AccountInternalUnfollower(val logger: Logger, userName: String, accountName: String)
    extends LoggerSupported {

  def execute = {
    val executor = new APIExecutor(logger, userName, accountName)
    executor.executeInternal { internalAPI =>
      debug("Start to follow")
      executor.getAccountStorage foreach { accountStorage =>
        accountStorage.getNotToUnFollowList match {
          case Some(notToUnfollowList) =>
            val toUnfollowPool = ListBuffer[Long]()
          // try to get peoples on unnfollow

          //              debug("Follow list contains: " + toFollowList.length)
          //              debug("Start to follow...")
          //              toFollowList.takeWhile { uid =>
          //                internalAPI.follow(uid) match {
          //                  case Some(followResult) =>
          //                    debug("User with id: " + uid + " - " + {
          //                      if (followResult.isFollowed)
          //                        "followed"
          //                      else if (followResult.isRequested)
          //                        "requested to follow!"
          //                      else
          //                        "HAVE UNCERTAIN TRUE RESULT!"
          //                    } + "!")
          //                    accountStorage.getEverFollowedList match {
          //                      case Some(everList) =>
          //                        if (everList add uid) {
          //                          debug("Try to add followed user " + uid + " to ever followed list")
          //                          if (everList.add(uid)) {
          //                            debug("User " + uid + " has been added to ever follow list!")
          //                            true
          //                          } else {
          //                            debug("Can't add user " + uid + " to ever followed list - returns false!")
          //                            false
          //                          }
          //                        } else {
          //                          debug("ERROR: Can't add user " + uid + " to ever followed list!")
          //                          false
          //                        }
          //                      case _ =>
          //                        err("ERROR: Can't get ever followed list.")
          //                        false
          //                    }
          //                    true
          //                  case _ =>
          //                    err("ERROR: User with id: " + uid + " - not followed. See error dump.")
          //                    false
          //                }
          //              }
          //              debug("Follow finished!")
          case _ =>
            err("Can't get to not to follow list!")
        }
      }
    }
  }

}