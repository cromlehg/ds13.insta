package ds13.insta.impl.rc1

import ds13.logger.Logger
import ds13.logger.LoggerSupported

class AccountWebFillToFollowList(val logger: Logger, userName: String, accountName: String, accountFromId: Long)
    extends LoggerSupported {

  def execute = {
    val executor = new APIExecutor(logger, userName, accountName)
    executor.executeWeb { webAPI =>
      if (webAPI.login) {
        debug("Login - OK")
        debug("Start to getting followes from " + accountFromId)
        executor.getAccountStorage foreach { accountStorage =>
          accountStorage.getToFollowList match {
            case Some(toFollowList) =>
              accountStorage.getNotToFollowList match {
                case Some(notToFollowList) =>
                  accountStorage.getEverFollowedList match {
                    case Some(everFollowedList) =>
                      debug("Follow list contains: " + toFollowList.length)
                      debug("Start to getting followers...")
                      val queriesLimit = 100
                      val size = 20
                      val queryNumbers = Array.range(0, queriesLimit - 1)
                      var cursor: Option[String] = None
                      queryNumbers.takeWhile { queryNumber =>
                        debug("Start query number: " + queryNumber)
                        (cursor match {
                          case Some(endCursor) => webAPI.getFollowedByAfter(accountFromId, size, endCursor)
                          case _               => webAPI.getFollowedByFirst(accountFromId, size)
                        }) match {
                          case Some(jsonObject) =>
                            val jsonObjFollowedBy = jsonObject.getJSONObject("followed_by")
                            val jsonObjNodes = jsonObjFollowedBy.getJSONArray("nodes")
                            val followedByList = for (i <- 0 until jsonObjNodes.length) yield jsonObjNodes.getJSONObject(i).getLong("id")
                            debug("Getting list with size " + followedByList.length)
                            debug("Filter it...")
                            val filtered = followedByList
                              .filterNot(notToFollowList.contains)
                              .filterNot(everFollowedList.contains)
                              .filterNot(notToFollowList.contains)
                              .filterNot(toFollowList.contains)
                            debug("List size after filtering " + followedByList.length)
                            debug("Save it to storage...")
                            val saved = toFollowList.add(followedByList)
                            debug("Saved " + saved)
                            if (saved != toFollowList.length)
                              warn("Saved szie and list size not equals, differs" + (toFollowList.length - saved))
                            debug("Check if needs to take next cursor " + saved)

                            val pageInfoJSONObj = jsonObjFollowedBy.getJSONObject("page_info")
                            val hasNextPage = pageInfoJSONObj.getBoolean("has_next_page")
                            if (hasNextPage) {
                              debug("Next page exist.")
                              val endCursor = pageInfoJSONObj.getString("end_cursor")
                              debug("Next page cursor - " + endCursor)
                              cursor = Some(endCursor)
                              true
                            } else {
                              debug("Next page not exists")
                              debug("Finished getting followed by accounts for " + accountFromId)
                              false
                            }
                          case _ =>
                            err("Some problems with getting folowers")
                            false
                        }
                      }
                      debug("Take followers finished.")
                    case _ => err("Can't get ever follow list!")
                  }
                case _ => err("Can't get not to follow list!")
              }
            case _ => err("Can't get to follow list!")
          }
        }
      } else err("Some problems with login")
    }
  }

}