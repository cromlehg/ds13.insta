package ds13.insta.bots

import scala.reflect.ClassTag

import org.json.JSONObject

import ds13.bots.AbstractBotDescriptor
import ds13.bots.AbstractBotHelper
import ds13.bots.BotController
import ds13.bots.TraitBotConfig
import ds13.bots.TraitBotProcessWithPause
import ds13.logger.Logger
import ds13.logger.PrintAllLogger
import ds13.bots.StateMachineBotProcess
import ds13.insta.impl.rc1.APIExecutor

abstract class InstaBotHelper[+R <: InstaBotResult, +C <: InstaBotProcessConfig, F <: InstaBotConfig, P <: InstaBotProcess[R, C]](logger: Logger, config: F)
  extends AbstractBotHelper[R, C, F, P](logger, config)

class InstaBotConfig extends TraitBotConfig

abstract class InstaBotDescriptor[+R <: InstaBotResult, +C <: InstaBotProcessConfig, F <: InstaBotConfig, P <: InstaBotProcess[R, C], H <: InstaBotHelper[R, C, F, P]](name: String)
  extends AbstractBotDescriptor[R, C, F, P, H](name)

class InstaBotResult

class InstaBotProcessConfig

class InstaBotProcess[+R <: InstaBotResult, +C <: InstaBotProcessConfig](override val logger: Logger, override val id: Long, val api: APIExecutor, val name: String)
    extends StateMachineBotProcess[R, C] {

  override def getPause: Long = 1000

  override def toJSON: Option[JSONObject] = None

  override def flushChanges: Unit =
    api.launchStorageFold(id, name)(err("Can't update state for " + id + ", " + name))(_ updateExtState toJSONString)

}

class InstaBotController[R <: InstaBotResult, C <: InstaBotProcessConfig, F <: InstaBotConfig: ClassTag, P <: InstaBotProcess[R, C], H <: InstaBotHelper[R, C, F, P]: ClassTag, D <: InstaBotDescriptor[R, C, F, P, H]]
  extends BotController[R, C, F, P, H, D]("InstaBotController")

object InstaBotConsts {

  val NAME = "Insta first bot"

}

object ExecutorTest1 extends App {

  val logger = PrintAllLogger()

  val api = new APIExecutor(logger, "iceberg", "cromlehg")

  val controller = new InstaBotController[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig], InstaBotHelper[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig]], InstaBotDescriptor[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig], InstaBotHelper[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig]]]]

  val testBotDescriptor = new InstaBotDescriptor[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig], InstaBotHelper[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig]]](InstaBotConsts.NAME) {

    override def newConfig: InstaBotConfig = new InstaBotConfig

    override def createHelper(config: InstaBotConfig): Option[InstaBotHelper[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig]]] =
      Some(new InstaBotHelper[InstaBotResult, InstaBotProcessConfig, InstaBotConfig, InstaBotProcess[InstaBotResult, InstaBotProcessConfig]](logger, config) {
        override protected def createBotProcess(config: InstaBotConfig, id: Long): Option[InstaBotProcess[InstaBotResult, InstaBotProcessConfig]] = {
          Some(new InstaBotProcess(logger, id, api, InstaBotConsts.NAME))
        }
      })

  }

  controller.addDescriptor(testBotDescriptor)

  controller.createBot(InstaBotConsts.NAME, 0) foreach { id =>
    controller.sendMsg(id, "start")
  }

}