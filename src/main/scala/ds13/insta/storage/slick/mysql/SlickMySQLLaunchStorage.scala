package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext

import ds13.http.TraitProxyProperties
import ds13.insta.storage.TraitAPIStorage
import ds13.insta.storage.TraitLaunchStorage
import javax.sql.rowset.serial.SerialBlob
import ds13.insta.storage.ConfigProvider
import java.io.File
import java.io.FileWriter

class SlickMySQLLaunchStorage(
  override val apiStorage: SlickMySQLAPIStorage,
  override val id: Long,
  override val name: String)
    extends TraitLaunchStorage with ResultSolver {

  override val logger = apiStorage.logger

  val dao = apiStorage.dao

  val configProvider = new ConfigProvider

  val path = configProvider.path

  val file = new File(path + "/" + id + "-state")

  def updateExtState(stateInfo: Option[String]): Unit =
    stateInfo.foreach { info =>
      new FileWriter(file) { write(info); close }
    }

}

