package ds13.insta.storage.slick.mysql

import ds13.insta.storage.TraitToFollowListStorage

class SlickMySQLToFollowListStorage(
  override val accountStorage: SlickMySQLAccountStorage,
  list: Seq[Long]) extends SlickMySQLListStorage(accountStorage, list)
    with TraitToFollowListStorage {

  override def add(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.addToToFollowList(aid, id)))) {
      listBuffer += id
      true
    } else false

  override def remove(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.removeFromToFollowList(aid, id)))) {
      listBuffer -= id
      true
    } else false

}

