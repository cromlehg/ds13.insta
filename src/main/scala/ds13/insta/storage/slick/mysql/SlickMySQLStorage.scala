package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import ds13.insta.storage.TraitModelFactory
import ds13.insta.storage.TraitStorage
import ds13.insta.storage.TraitUserStorage
import ds13.logger.Logger

class SlickMySQLStorage(override val logger: Logger) extends TraitStorage with ResultSolver {

  val dao = new SlickMySQLDAO(logger)

  val modelFactory = new SlickMySQLModelFactory(logger, this)

  override def getFutureUserStorageByLogin(login: String): Future[Option[TraitUserStorage]] =
    dao.getUserDataByLogin(login).map(_.map {
      case (id, login) => new SlickMySQLUserStorage(this, id, login)
    })

  override def getFutureUserStorages: Future[Seq[TraitUserStorage]] =
    dao.getUsersData.map(_ map {
      case (id, login) => new SlickMySQLUserStorage(this, id, login)
    })

  override def getUserStorages: Seq[TraitUserStorage] =
    solved(getFutureUserStorages)

  override def getUserStorageByLogin(login: String): Option[TraitUserStorage] =
    solved(getFutureUserStorageByLogin(login))

  override def dispose = dao.dispose

  override def factory: TraitModelFactory = modelFactory

}