package ds13.insta.storage.slick.mysql

import ds13.insta.storage.TraitEverFollowedListStorage

class SlickMySQLEverFollowedListStorage(
  override val accountStorage: SlickMySQLAccountStorage,
  list: Seq[Long]) extends SlickMySQLListStorage(accountStorage, list)
    with TraitEverFollowedListStorage {

  override def add(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.addToEverFollowedList(aid, id)))) {
      listBuffer += id
      true
    } else false

  override def remove(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.removeFromEverFollowedList(aid, id)))) {
      listBuffer -= id
      true
    } else false

}

