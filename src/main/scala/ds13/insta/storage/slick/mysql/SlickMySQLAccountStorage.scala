package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext
import ds13.insta.storage.TraitAPIStorage
import ds13.insta.storage.TraitAccountStorage
import ds13.logger.Logger
import ds13.insta.storage.TraitEverFollowedListStorage
import ds13.insta.storage.TraitNotToFollowListStorage
import ds13.insta.storage.TraitNotToUnFollowListStorage
import ds13.insta.storage.TraitToFollowListStorage

class SlickMySQLAccountStorage(
  override val userStorage: SlickMySQLUserStorage,
  override val id: Long,
  override val ownerId: Long,
  override val login: String,
  override val pass: String,
  override val accountId: Option[Long])
    extends TraitAccountStorage with ResultSolver {

  override val logger = userStorage.logger

  val dao = userStorage.dao

  override def getLogin: String = login

  override def getPass: String = pass

  override def getAccountId: Option[Long] = accountId

  override def getAPIStorageByNameAndVersion(name: String, version: String): Option[TraitAPIStorage] =
    solved(dao.getAPIStorageByNameAndVersionAndAccountId(name, version, id).map(_.map {
      case (id,
        accountId,
        name,
        version,
        deviceId,
        guid,
        proxyId) =>
        new SlickMySQLAPIStorage(
          this,
          id,
          accountId,
          name,
          version,
          deviceId,
          guid,
          proxyId)
    }))

  private def withAccountId[T](f: Long => Option[T]): Option[T] =
    accountId match {
      case Some(aid) =>
        f(aid)
      case _ =>
        err("Account ID is empty!")
        None
    }

  override def getToFollowList: Option[TraitToFollowListStorage] =
    withAccountId { aid =>
      Some(new SlickMySQLToFollowListStorage(this, solved(dao.getToFollowListByAccountId(aid))))
    }

  override def getEverFollowedList: Option[TraitEverFollowedListStorage] =
    withAccountId { aid =>
      Some(new SlickMySQLEverFollowedListStorage(this, solved(dao.getEverFollowedListByAccountId(aid))))
    }

  override def getNotToFollowList: Option[TraitNotToFollowListStorage] =
    withAccountId { aid =>
      Some(new SlickMySQLNotToFollowListStorage(this, solved(dao.getNotToFollowListByAccountId(aid))))
    }

  override def getNotToUnFollowList: Option[TraitNotToUnFollowListStorage] =
    withAccountId { aid =>
      Some(new SlickMySQLNotToUnFollowListStorage(this, solved(dao.getNotToUnFollowListByAccountId(aid))))
    }

  override def addToFollowList(ids: Seq[Long]): Int =
    withAccountId { aid =>
      Some(solvedInt(dao.addToToFollowList(aid, ids)))
    }.get

  override def addEverFollowedList(ids: Seq[Long]): Int =
    withAccountId { aid =>
      Some(solvedInt(dao.addToEverFollowedList(aid, ids)))
    }.get

  override def addNotToFollowList(ids: Seq[Long]): Int =
    withAccountId { aid =>
      Some(solvedInt(dao.addToNotToFollowList(aid, ids)))
    }.get

  override def addNotToUnFollowList(ids: Seq[Long]): Int =
    withAccountId { aid =>
      Some(solvedInt(dao.addToNotToUnFollowList(aid, ids)))
    }.get

  override def addToFollowList(id: Long): Boolean =
    withAccountId { aid =>
      Some(solved(dao.addToToFollowList(aid, id)))
    }.get

  override def addEverFollowedList(id: Long): Boolean =
    withAccountId { aid =>
      Some(solved(dao.addToEverFollowedList(aid, id)))
    }.get

  override def addNotToFollowList(id: Long): Boolean =
    withAccountId { aid =>
      Some(solved(dao.addToNotToFollowList(aid, id)))
    }.get

  override def addNotToUnFollowList(id: Long): Boolean =
    withAccountId { aid =>
      Some(solved(dao.addToNotToUnFollowList(aid, id)))
    }.get

}

