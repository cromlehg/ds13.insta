package ds13.insta.storage.slick.mysql

import slick.driver.MySQLDriver.api._
import java.sql.Blob

class Users(tag: Tag) extends Table[(Long, String)](tag, "users") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def login = column[String]("login")
  def * = (id, login)
}

class Proxies(tag: Tag) extends Table[(Long, String, Option[Int], Option[String], Option[String])](tag, "proxies") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def ip = column[String]("ip")
  def port = column[Option[Int]]("port")
  def login = column[Option[String]]("login")
  def pass = column[Option[String]]("pass")
  def * = (id, ip, port, login, pass)
}

class Accounts(tag: Tag) extends Table[(Long, Long, String, String, Option[Long])](tag, "accounts") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def ownerId = column[Long]("owner_id")
  def login = column[String]("login")
  def pass = column[String]("pass")
  def accountId = column[Option[Long]]("account_id")
  def * = (id, ownerId, login, pass, accountId)
}

class API(tag: Tag) extends Table[(Long, Long, String, String, Option[String], Option[String], Option[Long])](tag, "api") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def accountId = column[Long]("account_id")
  def name = column[String]("name")
  def version = column[String]("version")
  def deviceId = column[Option[String]]("device_id")
  def guid = column[Option[String]]("guid")
  def proxyId = column[Option[Long]]("proxy_id")
  def * = (id, accountId, name, version, deviceId, guid, proxyId)
}

class ListsNotUnfollow(tag: Tag) extends Table[(Long, Long)](tag, "lists_not_unfollow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId)
}

class ListsNotToFollow(tag: Tag) extends Table[(Long, Long)](tag, "lists_not_to_follow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId)
}

class ListsEverFollowed(tag: Tag) extends Table[(Long, Long)](tag, "lists_ever_followed") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId)
}

class ListsToFollow(tag: Tag) extends Table[(Long, Long)](tag, "lists_to_follow") {
  def accountId = column[Long]("account_id")
  def userId = column[Long]("user_id")
  def pk_a = primaryKey("pk_a", (accountId, userId))
  def * = (accountId, userId)
}

class Launches(tag: Tag) extends Table[(Long, String, Long, Int)](tag, "launches") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def botName = column[String]("bot_name")
  def apiId = column[Long]("api_id")
  def state = column[Int]("state")
  def * = (id, botName, apiId, state)
}
