package ds13.insta.storage.slick.mysql

import ds13.insta.storage.TraitNotToFollowListStorage

class SlickMySQLNotToFollowListStorage(
  override val accountStorage: SlickMySQLAccountStorage,
  list: Seq[Long]) extends SlickMySQLListStorage(accountStorage, list)
    with TraitNotToFollowListStorage {

  override def add(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.addToNotToFollowList(aid, id)))) {
      listBuffer += id
      true
    } else false

  override def remove(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.removeFromNotToFollowList(aid, id)))) {
      listBuffer -= id
      true
    } else false

}

