package ds13.insta.storage.slick.mysql

import ds13.insta.storage.TraitNotToUnFollowListStorage

class SlickMySQLNotToUnFollowListStorage(
  override val accountStorage: SlickMySQLAccountStorage,
  list: Seq[Long]) extends SlickMySQLListStorage(accountStorage, list)
    with TraitNotToUnFollowListStorage {

  override def add(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.addToNotToUnFollowList(aid, id)))) {
      listBuffer += id
      true
    } else false

  override def remove(id: Long): Boolean =
    if (withAccountIdBoolean(aid => solved(dao.removeFromNotToUnFollowList(aid, id)))) {
      listBuffer -= id
      true
    } else false

}

