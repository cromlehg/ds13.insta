package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext

import ds13.http.TraitProxyProperties
import ds13.insta.storage.TraitAPIStorage
import ds13.insta.storage.TraitLaunchStorage

class SlickMySQLAPIStorage(
    override val accountStorage: SlickMySQLAccountStorage,
    id: Long,
    accountId: Long,
    name: String,
    version: String,
    deviceId: Option[String],
    guid: Option[String],
    proxyId: Option[Long]) extends TraitAPIStorage with ResultSolver {

  override val logger = accountStorage.logger

  val dao = accountStorage.dao

  override def getProxyOpts: Option[TraitProxyProperties] =
    proxyId flatMap { prxId =>
      solved(dao.getProxyDataById(prxId).map(_.map {
        case (id, ip, port, login, pass) =>
          new SlickMySQLProxyProperties(logger, this, id, ip, port, login, pass)
      }))
    }

  override def getAccountId: Long = accountId

  override def getDeviceId: Option[String] = deviceId

  override def getGUID: Option[String] = guid

  override def getLaunchStorageByIdAndName(id: Long, name: String): Option[TraitLaunchStorage] =
    Some(new SlickMySQLLaunchStorage(this, id, name))

}

