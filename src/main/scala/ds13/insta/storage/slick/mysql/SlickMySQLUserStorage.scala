package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext
import ds13.insta.storage.TraitAccountStorage
import ds13.insta.storage.TraitUserStorage

class SlickMySQLUserStorage(
    override val storage: SlickMySQLStorage,
    override val id: Long,
    override val login: String) extends TraitUserStorage with ResultSolver {

  override val logger = storage.logger

  val dao = storage.dao

  override def getAccountStorageByLogin(login: String): Option[TraitAccountStorage] =
    solved(dao.getAccountDataByLogin(login).map(_.map {
      case (id,
        ownerId,
        login,
        pass,
        accountId) =>
        new SlickMySQLAccountStorage(
          this,
          id,
          ownerId,
          login,
          pass,
          accountId)
    }))

}

