package ds13.insta.storage.slick.mysql

import ds13.http.TraitProxyProperties
import ds13.logger.Logger

class SlickMySQLProxyProperties(
    override val logger: Logger,
    accountStorage: SlickMySQLAPIStorage,
    id: Long,
    ip: String,
    port: Option[Int],
    login: Option[String],
    pass: Option[String]) extends TraitProxyProperties {

  override def getIP: String = ip

  override def getPort: Option[Int] = port

  override def getLogin: Option[String] = login

  override def getPass: Option[String] = pass

}