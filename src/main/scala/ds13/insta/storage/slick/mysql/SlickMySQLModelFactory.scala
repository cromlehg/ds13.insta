package ds13.insta.storage.slick.mysql

import ds13.insta.storage.TraitModelFactory
import ds13.insta.storage.TraitUserStorage
import ds13.logger.Logger

class SlickMySQLModelFactory(
  override val logger: Logger,
  val storage: SlickMySQLStorage)
    extends TraitModelFactory {

  override def applyUserStorage(id: Long, login: String): TraitUserStorage =
    new SlickMySQLUserStorage(storage, id, login)

}