package ds13.insta.storage.slick.mysql

import scala.collection.mutable.ListBuffer

import ds13.insta.storage.TraitListStorage

abstract class SlickMySQLListStorage(
    val accountStorage: SlickMySQLAccountStorage,
    val list: Seq[Long]) extends TraitListStorage with ResultSolver {

  val listBuffer = ListBuffer() ++= list

  override val logger = accountStorage.logger

  val dao = accountStorage.dao

  override def takeWhile(f: Long => Boolean) = list takeWhile f

  override def length: Long = list.length

  override def contains(id: Long): Boolean = listBuffer contains id

  override def add(ids: Seq[Long]): Int = {
    var count = 0
    ids.foreach { id =>
      add(id)
      count += 1
    }
    count
  }

  protected def withAccountIdBoolean(f: Long => Boolean): Boolean =
    accountStorage.accountId match {
      case Some(aid) =>
        f(aid)
      case _ =>
        err("Account ID is empty in list storage action problem!")
        false
    }

  protected def withAccountIdInt(f: Long => Int): Int =
    accountStorage.accountId match {
      case Some(aid) =>
        f(aid)
      case _ =>
        err("Account ID is empty in list storage action problem!")
        0
    }

}

