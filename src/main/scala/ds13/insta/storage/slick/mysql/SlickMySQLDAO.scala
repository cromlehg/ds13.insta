package ds13.insta.storage.slick.mysql

import ds13.bots.CommmonImplicits.appContext
import scala.concurrent.Future

import ds13.logger.Logger
import ds13.logger.LoggerSupported
import slick.backend.DatabaseConfig
import slick.driver.MySQLDriver
import java.sql.Blob

class SlickMySQLDAO(override val logger: Logger) extends LoggerSupported {

  val config: DatabaseConfig[MySQLDriver] = DatabaseConfig.forConfig("slick.dbs.default")

  val db = config.db

  import config.driver.api._

  val users = TableQuery[Users]

  val proxies = TableQuery[Proxies]

  val accounts = TableQuery[Accounts]

  val api = TableQuery[API]

  val launches = TableQuery[Launches]

  val listsNotToUnFollow = TableQuery[ListsNotUnfollow]

  val listsNotToFollow = TableQuery[ListsNotToFollow]

  val listsEverFollowed = TableQuery[ListsEverFollowed]

  val listsToFollow = TableQuery[ListsToFollow]

  def dispose = db.close

  def getUsersData =
    db.run(users.result)

  def deleteLaunch(launchId: Long): Future[Boolean] =
    db.run(launches.filter(_.id === launchId).delete.transactionally) map { _ == 1 }

  def createLaunch(botName: String, apiId: Long, state: Int): Future[Option[(Long, String, Long, Int)]] = {
    val query = for {
      dbLaunch <- (launches returning launches.map(_.id) into ((v, id) => v.copy(_1 = id))) += (0, botName, apiId, state)
    } yield dbLaunch
    db.run(query.transactionally) map (s => Some((s._1, s._2, s._3, s._4)))
  }

  def updateLaunchState(launchId: Long, state: Int): Future[Boolean] =
    db.run(launches.filter(_.id === launchId).map(t => (t.state)).update(state).transactionally) map (r => if (r == 1) true else false)

  def getUserDataByLogin(login: String): Future[Option[(Long, String)]] =
    db.run(users.filter(_.login === login).result.headOption)

  def getAccountDataByLogin(login: String) =
    db.run(accounts.filter(_.login === login).result.headOption)

  def getProxyDataById(id: Long) =
    db.run(proxies.filter(_.id === id).result.headOption)

  def getAPIStorageByNameAndVersionAndAccountId(name: String, version: String, accountId: Long) =
    db.run(api.filter(_.name === name).filter(_.version === version).filter(_.accountId === accountId).result.headOption)

  def getToFollowListByAccountId(accountId: Long) =
    db.run(listsToFollow.filter(_.accountId === accountId).map(_.userId).result)

  def getNotToFollowListByAccountId(accountId: Long) =
    db.run(listsNotToFollow.filter(_.accountId === accountId).map(_.userId).result)

  def getNotToUnFollowListByAccountId(accountId: Long) =
    db.run(listsNotToUnFollow.filter(_.accountId === accountId).map(_.userId).result)

  def getEverFollowedListByAccountId(accountId: Long) =
    db.run(listsEverFollowed.filter(_.accountId === accountId).map(_.userId).result)

  def removeFromEverFollowedList(accountId: Long, toRemoveId: Long): Future[Boolean] =
    db.run(listsEverFollowed.filter(t => t.accountId === accountId && t.userId === toRemoveId).delete.transactionally) map (_ == 1)

  def removeFromNotToFollowList(accountId: Long, toRemoveId: Long): Future[Boolean] =
    db.run(listsNotToFollow.filter(t => t.accountId === accountId && t.userId === toRemoveId).delete.transactionally) map (_ == 1)

  def removeFromNotToUnFollowList(accountId: Long, toRemoveId: Long): Future[Boolean] =
    db.run(listsNotToUnFollow.filter(t => t.accountId === accountId && t.userId === toRemoveId).delete.transactionally) map (_ == 1)

  def removeFromToFollowList(accountId: Long, toRemoveId: Long): Future[Boolean] =
    db.run(listsToFollow.filter(t => t.accountId === accountId && t.userId === toRemoveId).delete.transactionally) map (_ == 1)

  def SQL_ListUpdate(listname: String) = "INSERT IGNORE INTO " + listname + " (account_id,user_id) VALUES (?, ?);"

  val SQL_EverFollowed = SQL_ListUpdate("lists_ever_followed")

  val SQL_ToFollow = SQL_ListUpdate("lists_to_follow")

  val SQL_NotToFollow = SQL_ListUpdate("lists_not_to_follow")

  val SQL_NotToUnfollow = SQL_ListUpdate("lists_not_unfollow")

  def addSimpleDBIOAction(stmt: String, accountId: Long, toAddId: Long) =
    db.run(SimpleDBIO[Boolean] { session =>
      val statement = session.connection.prepareStatement(stmt)
      statement.setLong(1, accountId)
      statement.setLong(2, toAddId)
      statement.executeUpdate > 0
    }.transactionally)

  def addToEverFollowedList(accountId: Long, toAddId: Long): Future[Boolean] =
    addSimpleDBIOAction(SQL_EverFollowed, accountId, toAddId)

  def addToToFollowList(accountId: Long, toAddId: Long): Future[Boolean] =
    addSimpleDBIOAction(SQL_ToFollow, accountId, toAddId)

  def addToNotToFollowList(accountId: Long, toAddId: Long): Future[Boolean] =
    addSimpleDBIOAction(SQL_NotToFollow, accountId, toAddId)

  def addToNotToUnFollowList(accountId: Long, toAddId: Long): Future[Boolean] =
    addSimpleDBIOAction(SQL_NotToUnfollow, accountId, toAddId)

  def addSimpleDBIOActionForList(stmt: String, accountId: Long, toAddIds: Seq[Long]) =
    db.run(SimpleDBIO[Int] { session =>
      val statement = session.connection.prepareStatement(stmt)
      var summary = 0
      toAddIds.grouped(50).foreach { group =>
        group.foreach { id =>
          statement.setLong(1, accountId)
          statement.setLong(2, id)
          statement.addBatch
        }
        summary += statement.executeBatch.length
      }
      summary
    }.transactionally)

  def addToEverFollowedList(accountId: Long, toAddIds: Seq[Long]): Future[Int] =
    addSimpleDBIOActionForList(SQL_EverFollowed, accountId, toAddIds)

  def addToToFollowList(accountId: Long, toAddIds: Seq[Long]): Future[Int] =
    addSimpleDBIOActionForList(SQL_ToFollow, accountId, toAddIds)

  def addToNotToFollowList(accountId: Long, toAddIds: Seq[Long]): Future[Int] =
    addSimpleDBIOActionForList(SQL_NotToFollow, accountId, toAddIds)

  def addToNotToUnFollowList(accountId: Long, toAddIds: Seq[Long]): Future[Int] =
    addSimpleDBIOActionForList(SQL_NotToUnfollow, accountId, toAddIds)

}