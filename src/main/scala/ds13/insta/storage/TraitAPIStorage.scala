package ds13.insta.storage

import ds13.http.TraitProxyProperties
import ds13.logger.LoggerSupported

trait TraitAPIStorage extends LoggerSupported {

  val accountStorage: TraitAccountStorage

  def getAccountId: Long

  def getProxyOpts: Option[TraitProxyProperties]

  def getDeviceId: Option[String]

  def getGUID: Option[String]

  def getPlatform: String =
    "Android (19/4.4.2; 160dpi; 720x1104; Samsung; GT-I9500; ja3g; unknown; en_EN)"

  def getAppVersion: String =
    "5.0.7"

  def getLaunchStorageByIdAndName(id: Long, name: String): Option[TraitLaunchStorage]

}