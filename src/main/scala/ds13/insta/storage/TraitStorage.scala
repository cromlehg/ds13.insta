package ds13.insta.storage

import ds13.logger.LoggerSupported
import scala.concurrent.Future

trait TraitStorage extends LoggerSupported {

  def getFutureUserStorageByLogin(login: String): Future[Option[TraitUserStorage]]

  def getFutureUserStorages: Future[Seq[TraitUserStorage]]

  def getUserStorageByLogin(login: String): Option[TraitUserStorage]

  def getUserStorages: Seq[TraitUserStorage]

  def factory: TraitModelFactory

  def dispose

}