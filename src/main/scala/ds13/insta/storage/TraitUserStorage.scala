package ds13.insta.storage

import ds13.logger.LoggerSupported

trait TraitUserStorage extends LoggerSupported {

  val id: Long

  val login: String

  val storage: TraitStorage

  def getAccountStorageByLogin(login: String): Option[TraitAccountStorage]

}