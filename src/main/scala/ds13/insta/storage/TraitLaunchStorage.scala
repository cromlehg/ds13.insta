package ds13.insta.storage

import ds13.http.TraitProxyProperties
import ds13.logger.LoggerSupported

trait TraitLaunchStorage extends LoggerSupported {

  val apiStorage: TraitAPIStorage

  val id: Long

  val name: String

  def updateExtState(extState: Option[String]): Unit

}