package ds13.insta.storage

import ds13.logger.LoggerSupported

trait TraitModelFactory extends LoggerSupported {

  def applyUserStorage(id: Long, login: String): TraitUserStorage

  def unapplyUserStorage(o: TraitUserStorage): Option[(Long, String)] =
    Some((o.id, o.login))

}