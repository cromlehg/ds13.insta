package ds13.insta.storage

import ds13.logger.LoggerSupported

trait TraitListStorage extends LoggerSupported {

  def length: Long

  def add(id: Long): Boolean

  def add(id: Seq[Long]): Int

  def remove(id: Long): Boolean

  def contains(id: Long): Boolean

  def takeWhile(f: Long => Boolean): Unit

}