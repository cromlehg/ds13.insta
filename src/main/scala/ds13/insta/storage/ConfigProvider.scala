package ds13.insta.storage

import com.typesafe.config.ConfigFactory

class ConfigProvider {

  val config = ConfigFactory.load

  val path = config.getString("ds13.bots.storage.logs")

}