package ds13.insta.storage

import ds13.logger.LoggerSupported
import scala.concurrent.Future

trait TraitAccountStorage extends LoggerSupported {

  val userStorage: TraitUserStorage

  val id: Long

  val ownerId: Long

  val login: String

  val pass: String

  val accountId: Option[Long]

  def getLogin: String

  def getPass: String

  def getAccountId: Option[Long]

  def getAPIStorageByNameAndVersion(name: String, version: String): Option[TraitAPIStorage]

  def getToFollowList: Option[TraitToFollowListStorage]

  def getEverFollowedList: Option[TraitEverFollowedListStorage]

  def getNotToFollowList: Option[TraitNotToFollowListStorage]

  def getNotToUnFollowList: Option[TraitNotToUnFollowListStorage]

  def addToFollowList(ids: Seq[Long]): Int

  def addEverFollowedList(ids: Seq[Long]): Int

  def addNotToFollowList(ids: Seq[Long]): Int

  def addNotToUnFollowList(ids: Seq[Long]): Int

  def addToFollowList(ids: Long): Boolean

  def addEverFollowedList(ids: Long): Boolean

  def addNotToFollowList(ids: Long): Boolean

  def addNotToUnFollowList(ids: Long): Boolean

}

