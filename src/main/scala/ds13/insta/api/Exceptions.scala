package ds13.insta.api

import org.json.JSONObject
import org.apache.http.client.methods.CloseableHttpResponse
import ds13.logger.Logger

// Request status and JSON statuses must be like trait composition

class DS13Exception extends Exception

class ClientException(val response: CloseableHttpResponse) extends DS13Exception {

  def dump(logger: Logger): Unit = {
    val statusLine = response.getStatusLine
    logger.err(statusLine.getStatusCode + ": " + statusLine.getReasonPhrase)
  }

}

class BadGatewayException(response: CloseableHttpResponse) extends ClientException(response)

class ServiceUnavaliableException(response: CloseableHttpResponse) extends ClientException(response)

class NotFoundException(response: CloseableHttpResponse) extends ClientException(response)

class InternalServerErrorException(response: CloseableHttpResponse) extends ClientException(response)

class BadRequestException(response: CloseableHttpResponse) extends ClientException(response)

class BadRequestExceptionWithStringContent(response: CloseableHttpResponse, val content: String) extends BadRequestException(response) {

  override def dump(logger: Logger): Unit = {
    super.dump(logger)
    logger.err(content)
  }

}

class BadRequestExceptionWithJSONContent(response: CloseableHttpResponse, val content: JSONObject) extends BadRequestException(response) {

  override def dump(logger: Logger): Unit = {
    super.dump(logger)
    logger.err(content.toString)
  }

}

class UnknownClientException(response: CloseableHttpResponse) extends ClientException(response)

class GetJSONContentException(response: CloseableHttpResponse) extends ClientException(response)

class JSONException(response: CloseableHttpResponse) extends ClientException(response)

class JSONInstException(response: CloseableHttpResponse, val json: JSONObject) extends JSONException(response) {

  override def dump(logger: Logger): Unit = {
    super.dump(logger)
    logger.err(json.toString)
  }

}

class JSONInstWihtoutStatus(response: CloseableHttpResponse, json: JSONObject) extends JSONInstException(response, json)

class JSONInstStatusIsNull(response: CloseableHttpResponse, json: JSONObject) extends JSONInstException(response, json)

class JSONInstStatusNotString(response: CloseableHttpResponse, json: JSONObject) extends JSONInstException(response, json)

class JSONInstUnknownStatus(response: CloseableHttpResponse, json: JSONObject, val status: String) extends JSONInstException(response, json) {

  override def dump(logger: Logger): Unit = {
    super.dump(logger)
    logger.err("JSON STATUS: " + status)
  }

}

class JSONInstFailStatus(response: CloseableHttpResponse, json: JSONObject, val errorType: Option[String]) extends JSONInstException(response, json) {

  val msg: Option[String] = if (json.has("message") && !json.isNull("message")) Some(json.getString("message")) else None

  override def dump(logger: Logger): Unit = {
    super.dump(logger)
    logger.err("Response in JSON format - Fail")
    errorType.foreach(t => logger.err("Error type: " + t))
    msg.foreach(t => logger.err("Message: " + t))
  }

}

class NotAuthorizedToViewUser(response: CloseableHttpResponse, json: JSONObject) extends JSONInstFailStatus(response, json, None)

class IncorrectPassword(response: CloseableHttpResponse, json: JSONObject) extends JSONInstFailStatus(response, json, Some("bad_password"))

class CheckpointLoggedOut(response: CloseableHttpResponse, json: JSONObject) extends JSONInstFailStatus(response, json, Some("checkpoint_logged_out")) {

  val lock = json getBoolean "lock"

  val checkpointUrl = json getString "checkpoint_url"

}

class CheckpointLoggedOutErrorWrongCode(response: CloseableHttpResponse, json: JSONObject, val code: Long) extends CheckpointLoggedOut(response, json)

class CheckpointLoggedOutErrorGetThridVerifyPage(response: CloseableHttpResponse, json: JSONObject) extends CheckpointLoggedOut(response, json)

class CheckpointLoggedOutErrorGetSecondVerifyPage(response: CloseableHttpResponse, json: JSONObject) extends CheckpointLoggedOut(response, json)

class CheckpointLoggedOutErrorGetVerifyPage(response: CloseableHttpResponse, json: JSONObject) extends CheckpointLoggedOut(response, json)

class CheckpointLoggedOutErrorGetToken(response: CloseableHttpResponse, json: JSONObject) extends CheckpointLoggedOut(response, json)
