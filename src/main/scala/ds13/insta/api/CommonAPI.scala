package ds13.insta.api

import org.apache.http.HttpStatus
import org.apache.http.client.methods.CloseableHttpResponse
import org.json.JSONObject

import ds13.http.HTTPClient
import ds13.http.HTTPClientConsts
import ds13.insta.storage.TraitAPIStorage
import org.apache.http.conn.ConnectTimeoutException
import org.apache.http.NoHttpResponseException
import scala.annotation.tailrec
import java.io.IOException
import org.apache.http.ParseException
import javax.net.ssl.SSLException
import org.apache.http.conn.HttpHostConnectException

class CommonAPI(apiStorage: TraitAPIStorage)
    extends HTTPClient(apiStorage.logger, apiStorage.getProxyOpts) {

  val attemptsLimitAfterIOException = 3

  var attemptsCounter = 0

  var lastError: Option[Error] = None

  var exceptionPause = 10000

  var exceptionPauseK = 2

  var tryAfterExceptionCounter = 0

  var tryAfterExceptionLimnit = 20

  override def handleError(code: Int, descrOpt: Option[String]) = {
    super.handleError(code, descrOpt)
    lastError = Some(new Error(code, descrOpt))
  }

  def infTryAfterException[T](f: => T): T =
    infTryAfterException(f, exceptionPause)

  final def infTryAfterException[T](f: => T, pause: Long): T =
    try f catch {
      case e @ (_: ServiceUnavaliableException |
        _: SSLException |
        _: BadGatewayException |
        _: InternalServerErrorException |
        _: HttpHostConnectException |
        _: ConnectTimeoutException |
        _: NoHttpResponseException) =>
        if (tryAfterExceptionCounter < tryAfterExceptionLimnit) {
          err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
          debug("Now I try to sleep about " + pause + "ms and try again... (the number of remaining attempts " + (tryAfterExceptionLimnit - tryAfterExceptionCounter) + ")")
          tryAfterExceptionCounter += 1
          Thread.sleep(pause)
          infTryAfterException(f, pause * exceptionPauseK)
        } else {
          debug("Limit reached")
          tryAfterExceptionCounter = 0
          throw e
        }
      case e: Throwable =>
        debug("Unprepared exception...")
        throw e
    }

  def sleepAndInfTry[T](f: => T, pause: Long): T = {
    Thread.sleep(pause)
    infTryAfterException(f, pause * exceptionPauseK)
  }

  def JSONInst[T](json: JSONObject, f: JSONObject => T)(implicit response: CloseableHttpResponse): T =
    if (json has "status") {
      if (json isNull "status") throw new JSONInstStatusIsNull(response, json) else {
        (json get "status") match {
          case status: String =>
            status.trim.toUpperCase match {
              case "OK"    => f(json)
              // Should throw exception in fail status, but exceptions must be concrete
              case unknown => throw new JSONInstUnknownStatus(response, json, unknown)
            }
          case _ => throw new JSONInstStatusNotString(response, json)
        }
      }
    } else throw new JSONInstWihtoutStatus(response, json)

  def okJSONInst[T](f: JSONObject => T)(implicit response: CloseableHttpResponse): T =
    okJSON(json => JSONInst(json, f))

  def okJSON[T](f: JSONObject => T)(implicit response: CloseableHttpResponse): T =
    ok(getJSONContent(response) match {
      case Some(json) => f(json)
      case _          => throw new GetJSONContentException(response)
    })

  def ok[T](f: => T)(implicit response: CloseableHttpResponse): T =
    response.getStatusLine.getStatusCode match {
      case HttpStatus.SC_OK                    => f
      case HttpStatus.SC_BAD_GATEWAY           => throw new BadGatewayException(response)
      // should try again later with middle timeout 
      case HttpStatus.SC_SERVICE_UNAVAILABLE   => throw new ServiceUnavaliableException(response)
      // sometimes occurs - should skip, and do next work 
      case HttpStatus.SC_NOT_FOUND             => throw new NotFoundException(response)
      // can be skipped  (preferably case) or try again
      case HttpStatus.SC_INTERNAL_SERVER_ERROR => throw new InternalServerErrorException(response)
      // POTENCIAL PROBLEM
      case HttpStatus.SC_BAD_REQUEST =>
        try getContent(response).fold(throw new BadRequestException(response))(_ match {
          case o: JSONObject =>
            if (o.has("error_type") && !o.isNull("error_type")) {
              o.getString("error_type") match {
                case "bad_password"          => throw new IncorrectPassword(response, o)
                case "checkpoint_logged_out" => throw new CheckpointLoggedOut(response, o)
                case et                      => throw new JSONInstFailStatus(response, o, Some(et))
              }
            } else if (o.has("message") && !o.isNull("message")) {
              o.getString("message") match {
                case "Not authorized to view user" => throw new NotAuthorizedToViewUser(response, o)
                case _                             => throw new JSONInstFailStatus(response, o, None)
              }
            } else
              throw new BadRequestExceptionWithJSONContent(response, o)
          case o: String => throw new BadRequestExceptionWithStringContent(response, o)
        }) catch {
          case e: IOException =>
            err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
            throw new BadRequestException(response)
          case e: ParseException =>
            err("EXCEPTION:\n\tmsg: " + e.getMessage + "\n\tclass: " + e.getClass.getSimpleName + "\n\tstack trace: \n\t" + e.getStackTrace.mkString("\n\t\t"))
            throw new BadRequestException(response)
          case e: Throwable => throw e
        }
      // .....
      case _ => throw new UnknownClientException(response)
    }

  def limitedAttemptIfIOException[T](ifYes: => Option[T])(ifNot: Int => Option[T]): Option[T] =
    if (isIOException) {
      debug("I/O Exception occurs - attempt again. Attemt counter: " + attemptsCounter)
      if (attemptsCounter < attemptsLimitAfterIOException) {
        attemptsCounter += 1
        val r = ifYes
        attemptsCounter = 0
        r
      } else {
        debug("Attemt counter limit exceeded: " + attemptsLimitAfterIOException)
        attemptsCounter = 0
        None
      }
    } else {
      debug("It's not I/O Exception!")
      attemptsCounter = 0
      processError(ifNot)
    }

  def limitedAttemptIfIOException(ifYes: => Boolean)(ifNot: Int => Boolean): Boolean =
    if (isIOException) {
      debug("I/O Exception occurs - attempt again. Attemt counter: " + attemptsCounter)
      if (attemptsCounter < attemptsLimitAfterIOException) {
        attemptsCounter += 1
        val r = ifYes
        attemptsCounter = 0
        r
      } else {
        debug("Attemt counter limit exceeded: " + attemptsLimitAfterIOException)
        attemptsCounter = 0
        false
      }
    } else {
      debug("It's not I/O Exception!")
      attemptsCounter = 0
      processError(ifNot)
    }

  def attemptIfIOException[T](ifYes: => Option[T])(ifNot: Int => Option[T]): Option[T] =
    if (isIOException) ifYes else processError(ifNot)

  def attemptIfIOException(ifYes: => Boolean)(ifNot: Int => Boolean): Boolean =
    if (isIOException) ifYes else processError(ifNot)

  def isIOException: Boolean =
    lastError.fold(false)(_.code == HTTPClientConsts.ERR_CODE_IO_EXCEPTION)

  def processError(f: Int => Unit) =
    lastError.foreach(e => f(e.code))

  def processError(f: Int => Boolean): Boolean =
    lastError.fold(false)(e => f(e.code))

  def processError[T](f: Int => Option[T]): Option[T] =
    lastError.flatMap(e => f(e.code))

  protected final def processResponseStringContentOk[T](response: CloseableHttpResponse)(onSuccess: String => Option[T]): Option[T] =
    processResponseOk(response)(processResponseContent(response, getStringContent(response))(onSuccess))

  protected final def processResponseOkJSONContent[T](response: CloseableHttpResponse)(onSuccess: JSONObject => Option[T]): Option[T] =
    processResponseOk(response)(processResponseJSONContent(response)(onSuccess))

  protected final def processResponseJSONContent[T](response: CloseableHttpResponse)(onSuccess: JSONObject => Option[T]): Option[T] =
    processResponseContent(response, getJSONContent(response)) { jsonObject =>
      if (jsonObject.getString("status") equalsIgnoreCase "OK")
        onSuccess(jsonObject)
      else
        None
    }

  protected final def processResponseContent[T, C](response: CloseableHttpResponse, bodyParser: Option[C])(onSuccess: C => Option[T]): Option[T] =
    bodyParser flatMap onSuccess

  protected final def processResponseOk[T](response: CloseableHttpResponse)(onSuccess: => Option[T]): Option[T] =
    processResponseByCode(response, HttpStatus.SC_OK)(onSuccess)

  protected final def processResponseMovedTemporary[T](response: CloseableHttpResponse)(onSuccess: => Option[T]): Option[T] =
    processResponseByCode(response, HttpStatus.SC_MOVED_TEMPORARILY)(onSuccess)

  protected final def processResponseByCode[T](response: CloseableHttpResponse, code: Int)(onSuccess: => Option[T]): Option[T] =
    if (response.getStatusLine.getStatusCode == code)
      onSuccess
    else {
      handleError(HTTPClientConsts.ERR_CODE_NOT_EXPECTED, Some(response.getStatusLine.getReasonPhrase))
      None
    }

}
