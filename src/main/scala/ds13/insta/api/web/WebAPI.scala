package ds13.insta.api.web

import org.json.JSONObject

import ds13.insta.api.CommonAPI
import ds13.insta.storage.TraitAPIStorage

class WebAPI(apiStorage: TraitAPIStorage)
    extends CommonAPI(apiStorage) {

  val URL = "https://www.instagram.com"

  val URL_LOGIN = URL + "/accounts/login/?force_classic_login"

  val URL_WEB = URL + "/web"

  val URL_WEB_LIKES = URL_WEB + "/likes/"

  val URL_WEB_COMMENTS = URL_WEB + "/comments/"

  val URL_WEB_FRIENDSHIPS = URL_WEB + "/friendships/"

  val LIKE = "/like/"

  val UNLIKE = "/unlike/"

  val ADD = "/add/"

  val DELETE = "/delete/"

  val FOLLOW = "/follow/"

  val UNFOLLOW = "/unfollow/"

  val csrfMiddleWareTokenPattern = "<input\\s*type\\s*=\\s*\"hidden\"\\s*name\\s*=\\s*\"csrfmiddlewaretoken\"\\s*value=\\s*\"([0-9a-zA-Z]+)\"\\s*\\/>".r

  val userIdPattern = "_auth_user_id%22%3A(\\d+)%2C%22_token".r

  val query_api_delimeter = "::"

  val query_relationships = "relationships"

  val query_follow_list = "follow_list"

  val query_relationships_follow_first = query_relationships + query_api_delimeter + query_follow_list

  val URL_QUERY = URL + "/query/"

  var userIdOpt: Option[Long] = None

  var isLoggedId = false

  addCommonHeaders(
    "Connection" -> "Keep-Alive",
    "Accept-Language" -> "ru-RU, en-US",
    "User-Agent" -> "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.100 Safari/537.36")

  def login: Boolean =
    if (isLoggedId) {
      debug("Already logged in!")
      true
    } else {
      debug("Web API start login")
      get(URL_LOGIN)
        .execute { preLoginResponse =>
          processResponseStringContentOk(preLoginResponse) { preLoginContent =>
            addCommonHeaders("referer" -> URL_LOGIN)
            post(URL_LOGIN)
              .withFormArgs(
                "username" -> apiStorage.accountStorage.getLogin,
                "password" -> apiStorage.accountStorage.getPass,
                "csrfmiddlewaretoken" -> csrfMiddleWareTokenPattern.findFirstMatchIn(preLoginContent).get.group(1))
              .execute { loginReponse =>
                processResponseMovedTemporary(loginReponse) {
                  getCookieValue("sessionid") match {
                    case Some(sessionId) =>
                      debug("SessionId cookie value: " + sessionId)
                      val userId = userIdPattern.findFirstMatchIn(sessionId).get.group(1)
                      debug("User ID: " + userId)
                      userIdOpt = Some(userId.toLong)
                      isLoggedId = true
                      getCookieValue("csrftoken") match {
                        case Some(csrftoken) =>
                          addCommonHeaders(
                            "X-Instagram-AJAX" -> "1",
                            "X-Requested-With" -> "XMLHttpRequest",
                            "X-CSRFToken" -> csrftoken)
                          debug("Web API login - OK")
                          Some(true)
                        case _ =>
                          err("Can't get CSRF token from login post request cookie!")
                          None
                      }
                    case _ =>
                      err("Can't get session id from login post request!")
                      None
                  }
                }
              }
          }
        } match {
          case Some(result) => result
          case _            => false
        }
    }

  def follow(userId: String): Boolean = {
    info("Try to follow to user with id " + userId)
    post(URL_WEB_FRIENDSHIPS + userId + FOLLOW)
      .execute { response =>
        processResponseOk(response) {
          info("Follow to user " + userId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  def unfollow(userId: String): Boolean = {
    info("Try to unfollow to user with id " + userId)
    post(URL_WEB_FRIENDSHIPS + userId + UNFOLLOW)
      .execute { response =>
        processResponseOk(response) {
          info("Unfollow from user " + userId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  def like(postId: String): Boolean = {
    info("Try to like post with id " + postId)
    post(URL_WEB_LIKES + postId + LIKE)
      .execute { response =>
        processResponseOk(response) {
          info("Like post " + postId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  def unlike(postId: String): Boolean = {
    info("Try to unlike post with id " + postId)
    post(URL_WEB_LIKES + postId + UNLIKE)
      .execute { response =>
        processResponseOk(response) {
          info("Unlike post " + postId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  def addComment(postId: String, comment: String): Boolean = {
    info("Try to comment post with id " + postId)
    post(URL_WEB_COMMENTS + postId + ADD)
      .withFormArgs("comment_text" -> comment)
      .execute { response =>
        processResponseOk(response) {
          info("Add comment to post " + postId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  def removeComment(postId: String, comment: String): Boolean = {
    info("Try to remove comment from post with id " + postId)
    post(URL_WEB_COMMENTS + postId + DELETE)
      .execute { response =>
        processResponseOk(response) {
          info("Remove comment from post " + postId + " - OK!")
          Some(true)
        }
      } match {
        case Some(result) => result
        case _            => false
      }
  }

  // like and unlike
  //https://www.instagram.com/web/likes/1347869791259748878/like/

  def getFollowsFirst(userId: Long, size: Int) = {
    info("getFollowFirst for userId = " + userId + ", with size = " + size)
    executeReadQuery(new IARIGUserFollowsFirstFunctionRequestDefault(userId, size), query_relationships_follow_first, "follows")
  }

  def getFollowsAfter(userId: Long, size: Int, cursor: String) = {
    info("getFollowAfter for userId = " + userId + ", with size = " + size + ", and cursor = " + cursor)
    executeReadQuery(new IARIGUserFollowsAfterFunctionRequestDefault(userId, size, cursor), query_relationships_follow_first, "follows")
  }

  def getFollowedByFirst(userId: Long, size: Int) = {
    info("getFollowedByFirst for userId = " + userId + ", with size = " + size)
    executeReadQuery(new IARIGUserFollowedByFirstFunctionRequestDefault(userId, size), query_relationships_follow_first, "followed_by")
  }

  def getFollowedByAfter(userId: Long, size: Int, cursor: String) = {
    info("getFollowedByAfter for userId = " + userId + ", with size = " + size + ", and cursor = " + cursor)
    executeReadQuery(new IARIGUserFollowedByAfterFunctionRequestDefault(userId, size, cursor), query_relationships_follow_first, "followed_by")
  }

  def getPostsOwnersByHashtagFirst(hashtag: String, size: Int) = {
    info("getPostsOwnersByHashtagFirst for hashtag = " + hashtag + ", with size = " + size)
    executeReadQuery("""
ig_hashtag(""" + hashtag + """) { media.first(""" + size + """) {
  nodes {
    owner {
      id
    }
  },
  page_info
}           
}
      """, "tags::show", "media")
  }

  def getPostsOwnersByHashtagAfter(hashtag: String, size: Int, cursor: String) = {
    info("getPostsOwnersByHashtagAfter for hashtag = " + hashtag + ", with size = " + size)
    executeReadQuery("""
ig_hashtag(""" + hashtag + """) { media.after(""" + cursor + """, """ + size + """) {
  nodes {
    owner {
      id
    }
  },
  page_info
}           
}
      """, "tags::show", "media")
  }

  private def executeReadQuery(q: RequestNode, ref: String, nodeName: String): Option[JSONObject] =
    executeReadQuery(q.toString, ref, nodeName)

  private def executeReadQuery(q: String, ref: String, nodeName: String): Option[JSONObject] = {
    debug("Execute read query: " + q + ", with ref = " + ref)
    post(URL_QUERY)
      .withFormArgs(
        "q" -> q,
        "ref" -> ref)
      .execute { response =>
        processResponseOkJSONContent(response) { jsonObject =>
          if (jsonObject has nodeName) {
            err("JSON object with name " + nodeName + " - getting Ok!")
            Some(jsonObject.getJSONObject(nodeName))
          } else {
            err("JSON node with name " + nodeName + " - not exists in response!")
            None
          }
        }
      }
  }

}

object WebAPIConsts {

  val NAME = "Web"

  val VERSION = "1"

}