package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.logger.TraitJSON

class FeedItem(
  val commentCount: Int,
  val code: String,
  val likeCount: Int,
  val hasLiked: Boolean,
  val comments: Seq[Comment],
  val photoOfYou: Boolean,
  val previewComments: Seq[Comment],
  val caption: Option[Comment],
  val organicTrackingToken: String,
  val topLikers: Seq[String],
  val clientCacheKey: String,
  val maxNumVisiblePreviewComments: Option[Int],
  val takenAt: Long,
  val mediaType: Int,
  val filterType: Int,
  val deviceTimestamp: Long,
  val cpationIsEdited: Boolean,
  val pk: Long,
  val id: String,
  val nextMaxId: Option[String],
  val imageVersions: Seq[ImageVersion],
  val user: User,
  val hasMoreComments: Boolean)
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("comment_count", commentCount)
      accumulate("code", code)
      accumulate("like_count", likeCount)
      accumulate("has_liked", hasLiked)
      comments.foreach(_.toJSON.foreach(u => append("comments", u)))
      accumulate("photo_of_you", photoOfYou)
      previewComments.foreach(_.toJSON.foreach(u => append("preview_comments", u)))
      caption foreach (_.toJSON foreach (t => accumulate("caption", t)))
      accumulate("organic_tracking_token", organicTrackingToken)
      topLikers.foreach(t => append("top_likers", t))
      accumulate("client_cache_key", clientCacheKey)
      maxNumVisiblePreviewComments foreach { t => accumulate("max_num_visible_preview_comments", t) }
      accumulate("taken_at", takenAt)
      accumulate("media_type", mediaType)
      accumulate("filter_type", filterType)
      accumulate("device_timestamp", deviceTimestamp)
      accumulate("caption_is_edited", cpationIsEdited)
      accumulate("pk", pk)
      accumulate("id", id)
      nextMaxId.foreach(t => accumulate("next_max_id", t))
      imageVersions.foreach(_.toJSON.foreach(t => append("image_versions", t)))
      user.toJSON.foreach(t => accumulate("user", t))
      accumulate("has_more_comments", hasMoreComments)
    })

}
