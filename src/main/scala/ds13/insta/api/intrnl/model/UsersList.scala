package ds13.insta.api.intrnl.model

import org.json.JSONObject

object EmptyUsersList {

  def apply(): UsersList = new UsersList(None, Seq.empty, false, 0)

}

class UsersList(
  val nextMaxIdOpt: Option[String],
  val users: Seq[User],
  val isBigList: Boolean,
  val pageSize: Int)
    extends TraitUsersContainerWithNextMaxId {

  override def getSize: Int = users.length

  override def getUser(index: Int) = users(index)

  override def getNextMaxId: Option[String] = nextMaxIdOpt

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      nextMaxIdOpt.foreach(t => accumulate("next_max_id", t))
      users.foreach(_.toJSON.foreach(u => append("users", u)))
      accumulate("big_list", isBigList)
      accumulate("page_size", pageSize)
    })

}

