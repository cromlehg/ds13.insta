package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.logger.TraitJSON

class FBLocation(
  val subtitle: String,
  val location: Location,
  val title: String,
  val mediaBundles: Seq[MediaBundle])
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("subtitle", subtitle)
      accumulate("location", location)
      accumulate("title", title)
      mediaBundles.foreach(_.toJSON.foreach(u => append("media_bundles", u)))
    })

}