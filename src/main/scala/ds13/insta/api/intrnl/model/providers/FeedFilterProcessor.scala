package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.Feed
import ds13.insta.api.intrnl.model.User

class FeedFilterProcessor extends TraitFilterProcessor[Feed] {

  override def createFiltered(feed: Feed, f: TraitFilter[User]): Feed =
    new Feed(
      feed.isCommentLikesEnabled,
      feed.isMoreAvailable,
      feed.nextMaxIdOpt,
      feed.isAutoLoadMoreEnabled,
      feed.resultsCount,
      feed.items.filterNot(f skip _.user),
      feed.rankedItems)

}