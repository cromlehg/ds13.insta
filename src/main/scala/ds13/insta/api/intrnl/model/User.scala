package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.logger.TraitJSON

class User(
  val username: String,
  val id: Long,
  val isPrivate: Boolean,
  val isVerified: Option[Boolean],
  val isUnpublished: Option[Boolean],
  val isFavorite: Option[Boolean],
  val profilePicId: Option[String],
  val hasAnonymousProfilePicture: Option[Boolean],
  val profilePicUrl: Option[String],
  val fullName: String,
  val allowContactsSync: Option[Boolean],
  val showFeedBizConversionIcon: Option[Boolean])
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("username", username)
      accumulate("pk", id)
      accumulate("is_private", isPrivate)
      isVerified.foreach(t => accumulate("is_verified", t))
      isUnpublished.foreach(t => accumulate("is_unpublished", t))
      isFavorite.foreach(t => accumulate("is_favorite", t))
      accumulate("full_name", fullName)
      profilePicId.foreach(t => accumulate("profile_pic_id", t))
      hasAnonymousProfilePicture.foreach(t => accumulate("has_anonymous_profile_picture", t))
      profilePicUrl.foreach(t => accumulate("profile_pic_url", t))
      allowContactsSync.foreach(t => accumulate("allow_contacts_sync", t))
      showFeedBizConversionIcon.foreach(t => accumulate("show_feed_biz_conversion_icon", t))
    })

}

