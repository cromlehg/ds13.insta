package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.intrnl.InternalAPI
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.providers.TraitFilter

trait TraitUsersContainerWithMaxIdUsersProvider extends TraitUsersProvider {

  var opt: Option[TraitUsersContainerWithNextMaxId] = None

  var index = 0

  def updateOpt[T](nextMaxIdOpt: Option[String])(f: TraitUsersContainerWithNextMaxId => T)(implicit filter: Option[TraitFilter[User]]): T = {
    index = 0
    opt = Some(api.internalFold[TraitUsersContainerWithNextMaxId](emptyContainer) { iapi =>
      getNextContainer(iapi, nextMaxIdOpt)
    })
    f(opt.get)
  }

  def emptyContainer: TraitUsersContainerWithNextMaxId

  override def hasNext()(implicit filter: Option[TraitFilter[User]]): Boolean =
    opt.fold(updateOpt(None)(_.getSize > 0)) { container =>
      index < container.getSize ||
        container.getNextMaxId.fold(false)(nextMaxIdOpt => updateOpt(Some(nextMaxIdOpt))(_ => hasNext))
    }

  override def apply[T](f: User => T): T =
    f(opt.get.getUser {
      index += 1
      index - 1
    })

  def getNextContainer(iapi: InternalAPI, nextMaxId: Option[String])(implicit filter: Option[TraitFilter[User]]): TraitUsersContainerWithNextMaxId

  override def getJSONState: Option[JSONObject] =
    TraitUsersContainerWithMaxIdUsersProvider.this.opt.flatMap(_.toJSON.map { t =>
      new JSONObject {
        accumulate("container", t)
        accumulate("index", index)
      }
    })

}