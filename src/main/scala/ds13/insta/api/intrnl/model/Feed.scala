package ds13.insta.api.intrnl.model

import org.json.JSONObject

object EmptyFeed {

  def apply() =
    new Feed(None, false, None, false, 0, Seq.empty, Seq.empty)

}

class Feed(
  val isCommentLikesEnabled: Option[Boolean],
  val isMoreAvailable: Boolean,
  val nextMaxIdOpt: Option[String],
  val isAutoLoadMoreEnabled: Boolean,
  val resultsCount: Int,
  val items: Seq[FeedItem],
  val rankedItems: Seq[FeedItem])
    extends TraitUsersContainerWithNextMaxId {

  override def getSize: Int = items.length

  override def getUser(index: Int) = items(index).user

  override def getNextMaxId: Option[String] = nextMaxIdOpt

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      isCommentLikesEnabled.foreach(t => accumulate("comment_likes_enabled", t))
      accumulate("more_available", isMoreAvailable)
      nextMaxIdOpt.foreach(t => accumulate("next_max_id", t))
      accumulate("auto_load_more_enabled", isAutoLoadMoreEnabled)
      accumulate("num_results", resultsCount)
      items.foreach(_.toJSON.foreach(u => append("items", u)))
      rankedItems.foreach(_.toJSON.foreach(u => append("ranked_items", u)))
    })

}