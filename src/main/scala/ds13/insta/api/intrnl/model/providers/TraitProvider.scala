package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.User
import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.TraitJSON
import org.json.JSONObject

trait TraitProvider[M] extends TraitJSON {

  val name: String

  val api: APIExecutor

  def hasNext()(implicit filter: Option[TraitFilter[M]] = None): Boolean

  def apply[T](f: M => T): T

  def fold[T](isEmpty: => T)(f: M => T)(implicit filter: Option[TraitFilter[M]] = None): T =
    if (hasNext) apply(f) else isEmpty

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("name", name)
      getJSONState.foreach(s => accumulate("state", s))
    })

  def getJSONState: Option[JSONObject] = None

}