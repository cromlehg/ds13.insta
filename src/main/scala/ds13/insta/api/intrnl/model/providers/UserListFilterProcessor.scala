package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.UsersList
import ds13.insta.api.intrnl.model.User

class UserListFilterProcessor extends TraitFilterProcessor[UsersList] {

  override def createFiltered(usersList: UsersList, f: TraitFilter[User]): UsersList =
    new UsersList(
      usersList.nextMaxIdOpt,
      usersList.users.filterNot(f skip _),
      usersList.isBigList,
      usersList.pageSize)

}