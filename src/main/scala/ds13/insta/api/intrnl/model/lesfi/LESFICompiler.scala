package ds13.insta.api.intrnl.model.lesfi

import ds13.insta.api.intrnl.model.providers.users.CompositeUsersProvider
import ds13.insta.api.intrnl.model.providers.users.HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider
import ds13.insta.api.intrnl.model.providers.users.HashtagFeedItemsOwnersUsersProvider
import ds13.insta.api.intrnl.model.providers.users.TraitUsersProvider
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import ds13.insta.impl.rc1.APIExecutor

class LESFICompiler {

  def splitted[T](lesfi: String)(f: Array[String] => T): T =
    f(lesfi.split(" ").map(_.trim).filterNot(_.length == 0))

  def checks(lexes: Array[String]): Boolean =
    lexes.find(_.length < 2).isEmpty &&
      lexes.find(t => !(t.startsWith("#") || t.startsWith("@") || t.startsWith("&"))).isEmpty // TODO: Long checks

  def checks(lesfi: String): Boolean = splitted(lesfi)(checks)

  def checked(lesfi: String)(f: Array[String] => TraitUsersProvider): Option[TraitUsersProvider] =
    splitted(lesfi)(values => if (checks(values)) Some(f(values)) else None)

  def parseProvider(api: APIExecutor, lesfi: String): Option[TraitUsersProvider] =
    checked(lesfi)(values => new CompositeUsersProvider(api, (values.map { value =>
      if (value startsWith "#")
        new HashtagFeedItemsOwnersUsersProvider(api, value.substring(1).trim)
      else if (value startsWith "&")
        new HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(api, value.substring(1).trim.toLong)
      else
        new UserFollowingsUsersProvider(api, value.substring(1))
    }): _*))

}

