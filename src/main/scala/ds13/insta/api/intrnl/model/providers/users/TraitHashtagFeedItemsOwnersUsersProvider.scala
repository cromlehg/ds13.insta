package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.intrnl.InternalAPI
import ds13.insta.api.intrnl.model.EmptyFeed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.impl.rc1.APIExecutor

trait TraitHashtagFeedItemsOwnersUsersProvider
    extends TraitUsersContainerWithMaxIdUsersProvider {

  override def emptyContainer: TraitUsersContainerWithNextMaxId =
    EmptyFeed()

}