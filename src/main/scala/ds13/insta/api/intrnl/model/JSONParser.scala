package ds13.insta.api.intrnl.model

import org.json.JSONArray
import org.json.JSONObject

import ds13.insta.api.intrnl.model.providers.users.CompositeUsersProvider
import ds13.insta.api.intrnl.model.providers.users.HashtagFeedItemsOwnersUsersProvider
import ds13.insta.api.intrnl.model.providers.users.TraitUsersContainerWithMaxIdUsersProvider
import ds13.insta.api.intrnl.model.providers.users.TraitUsersProvider
import ds13.insta.api.intrnl.model.providers.users.UserFollowingsUsersProvider
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.api.intrnl.model.providers.users.HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider

class JSONParser {

  def parseOptInt(o: JSONObject, id: String) =
    if ((o has id) && !o.isNull(id)) Some(o getInt id) else None

  def parseOptLong(o: JSONObject, id: String) =
    if ((o has id) && !o.isNull(id)) Some(o getLong id) else None

  def parseOptBoolean(o: JSONObject, id: String) =
    if ((o has id) && !o.isNull(id)) Some(o getBoolean id) else None

  def parseOptString(o: JSONObject, id: String) =
    if ((o has id) && !o.isNull(id)) Some(o getString id) else None

  def parseOptStringFromStringLong(o: JSONObject, id: String) =
    if ((o has id) && !o.isNull(id)) Some((o get id).toString) else None

  def parseNextMaxIdOpt(o: JSONObject) =
    parseOptStringFromStringLong(o, "next_max_id")

  def parseUsers(o: JSONArray) =
    for (i <- 0 until o.length) yield parseUser(o getJSONObject i)

  def parseUserOptIfOk(o: JSONObject) =
    parseOpt(o, "user", parseUser)

  def parseUserContainedLoggedInUser(o: JSONObject) =
    parseUserContained(o, "logged_in_user")

  def parseUserContainedUser(o: JSONObject) =
    parseUserContained(o, "user")

  def parseUserContained(o: JSONObject, id: String) =
    parseUser(o getJSONObject id)

  def parseUserOpt(o: JSONObject) =
    Some(parseUser(o))

  def parseMediaBudles(o: JSONArray) =
    Seq.empty[MediaBundle]
  //for (i <- 0 until o.length) yield parseMediaBudle(o getJSONObject i)

  def parseFBLocationsListItems(o: JSONArray) =
    for (i <- 0 until o.length) yield parseFBLocation(o getJSONObject i)

  def parseFBLocationList(o: JSONObject) =
    new FBLocationList(
      parseOptBoolean(o, "has_more"),
      parseArray(o, "items", parseFBLocationsListItems))

  def parseFBLocation(o: JSONObject) =
    new FBLocation(
      o getString "subtitle",
      parseLocation(o getJSONObject "location"),
      o getString "title",
      parseArray(o, "media_bundles", parseMediaBudles))

  def parseLocation(o: JSONObject) =
    new Location(
      o getString "address",
      o getDouble "lng",
      o getString "city",
      o getLong "facebook_places_id",
      o getString "name",
      o getLong "pk",
      o getDouble "lat",
      o getString "external_source")

  def parseUser(o: JSONObject) =
    new User(
      o getString "username",
      o getLong "pk",
      o getBoolean "is_private",
      parseOptBoolean(o, "is_verified"),
      parseOptBoolean(o, "is_unpublished"),
      parseOptBoolean(o, "is_favorite"),
      parseOptString(o, "profile_pic_id"),
      parseOptBoolean(o, "has_anonymous_profile_picture"),
      parseOptString(o, "profile_pic_url"),
      o getString "full_name",
      parseOptBoolean(o, "allow_contacts_sync"),
      parseOptBoolean(o, "show_feed_biz_conversion_icon"))

  def parseOptUsersList(o: JSONObject): Option[UsersList] =
    Some(parseUsersList(o))

  def parseUsersList(o: JSONObject): UsersList =
    new UsersList(
      parseNextMaxIdOpt(o),
      parseArray(o, "users", parseUsers),
      o getBoolean "big_list",
      o getInt "page_size")

  def parseOptFeed(o: JSONObject) =
    Some(parseFeed(o))

  def parseArray[T](o: JSONObject, id: String, f: JSONArray => Seq[T]): Seq[T] =
    if ((o has id) && !o.isNull(id)) f(o getJSONArray id) else Seq.empty

  def parseOpt[T](o: JSONObject, id: String, f: JSONObject => T): Option[T] =
    if ((o has id) && !o.isNull(id)) Some(f(o getJSONObject id)) else None

  def parseFeed(o: JSONObject) =
    new Feed(
      parseOptBoolean(o, "comment_likes_enabled"),
      o getBoolean "more_available",
      parseNextMaxIdOpt(o),
      o getBoolean "auto_load_more_enabled",
      o getInt "num_results",
      parseArray(o, "items", parseFeedItems),
      parseArray(o, "ranked_items", parseFeedItems))

  def parseFeedItems(o: JSONArray) =
    for (i <- 0 until o.length) yield parseFeedItem(o getJSONObject i)

  def parseStrings(o: JSONArray) =
    for (i <- 0 until o.length) yield o getString i

  def parseFeedItem(o: JSONObject) =
    new FeedItem(
      o getInt "comment_count",
      o getString "code",
      o getInt "like_count",
      o getBoolean "has_liked",
      parseArray(o, "comments", parseComments),
      o getBoolean "photo_of_you",
      parseArray(o, "preview_comments", parseComments),
      parseOpt(o, "caption", parseComment),
      o getString "organic_tracking_token",
      parseArray(o, "top_likers", parseStrings),
      o getString "client_cache_key",
      parseOptInt(o, "max_num_visible_preview_comments"),
      o getLong "taken_at",
      o getInt "media_type",
      o getInt "filter_type",
      o getLong "device_timestamp",
      o getBoolean "caption_is_edited",
      o getLong "pk",
      o getString "id",
      parseNextMaxIdOpt(o),
      parseImageVerions(o getJSONArray "image_versions"),
      parseUser(o getJSONObject "user"),
      o getBoolean "has_more_comments")

  def parseImageVerions(o: JSONArray) =
    for (i <- 0 until o.length) yield parseImageVersion(o getJSONObject i)

  def parseImageVersion(o: JSONObject) =
    new ImageVersion(
      o getInt "width",
      o getInt "height",
      o getInt "type",
      o getString "url")

  def parseComments(o: JSONArray) =
    for (i <- 0 until o.length) yield parseComment(o getJSONObject i)

  def parseComment(o: JSONObject) =
    new Comment(
      o getLong "created_at_utc",
      o getString "content_type",
      o getLong "user_id",
      o getInt "bit_flags",
      o getLong "created_at",
      o getLong "media_id",
      o getString "text",
      o getLong "pk",
      o getInt "type",
      parseUser(o getJSONObject "user"),
      o getString "status")

  def parseFollowOperationInfo(o: JSONObject) =
    new FollowOperationInfo(
      o getBoolean "following",
      o getBoolean "outgoing_request",
      o getBoolean "is_private",
      o getBoolean "blocking")

  def parseFriendshipStatusOpt(o: JSONObject) =
    Some(parseFriendshipStatus(o))

  def parseFriendshipStatus(o: JSONObject) =
    parseFollowOperationInfo(o getJSONObject "friendship_status")

  def parseUsersProvider(api: APIExecutor, o: JSONObject) =
    (o getString "name") match {
      case CompositeUsersProvider.NAME => parseCompositeUsersProvider(api, o)
      case UserFollowingsUsersProvider.NAME => parseUserFollowingsUsersProvider(api, o)
      case HashtagFeedItemsOwnersUsersProvider.NAME => parseHashtagFeedItemsOwnersUsersProvider(api, o)
      case HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider.NAME => parseHashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(api, o)
      case name => throw new UnsupportedOperationException("Uunknwon provider name: " + name)
    }

  def parseCompositeUsersProvider(api: APIExecutor, o: JSONObject): CompositeUsersProvider =
    parseUsersProviderMod(api, o)((api, oOpt) => parseCompositeUsersProviderState(api, oOpt))

  def parseUserFollowingsUsersProvider(api: APIExecutor, o: JSONObject): UserFollowingsUsersProvider =
    parseUsersProviderMod(api, o)((api, oOpt) => parseUserFollowingsUsersProviderState(api, oOpt.get))

  def parseHashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(api: APIExecutor, o: JSONObject): HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider =
    parseUsersProviderMod(api, o)((api, oOpt) => parseHashtagFeedItemsOwnersFromSingleLocationIdUsersProviderState(api, oOpt.get))

  def parseHashtagFeedItemsOwnersUsersProvider(api: APIExecutor, o: JSONObject): HashtagFeedItemsOwnersUsersProvider =
    parseUsersProviderMod(api, o)((api, oOpt) => parseHashtagFeedItemsOwnersUsersProviderState(api, oOpt.get))

  def parseUsersProviderMod[T <: TraitUsersProvider](api: APIExecutor, o: JSONObject)(stateParser: (APIExecutor, Option[JSONObject]) => T): T =
    stateParser(api, { if ((o has "state") && !(o isNull "state")) Some(o getJSONObject "state") else None })

  def parseHashtagFeedItemsOwnersUsersProviderState(api: APIExecutor, o: JSONObject) =
    parseUserContainerWithMaxIdState(o)(new HashtagFeedItemsOwnersUsersProvider(api, o getString "hashtag"), parseFeed)

  def parseHashtagFeedItemsOwnersFromSingleLocationIdUsersProviderState(api: APIExecutor, o: JSONObject) =
    parseUserContainerWithMaxIdState(o)(new HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(api, o getLong "location_id"), parseFeed)

  def parseUserFollowingsUsersProviderState(api: APIExecutor, o: JSONObject) =
    parseUserContainerWithMaxIdState(o)(
      new UserFollowingsUsersProvider(api, o getString "username") {
        userIdOpt = parseOptLong(o, "user_id")
      }, parseUsersList)

  def parseUserContainerWithMaxIdState[T <: TraitUsersContainerWithMaxIdUsersProvider, M <: TraitUsersContainerWithNextMaxId](o: JSONObject)(specificParser: => T, containerParser: JSONObject => M): T = {
    val r = specificParser
    if (o has "container")
      r.opt = Some {
        r.index = o getInt "index"
        containerParser(o getJSONObject "container")
      }
    r
  }

  def parseCompositeUsersProviderState(api: APIExecutor, oOpt: Option[JSONObject]) =
    oOpt match {
      case Some(o) =>
        new CompositeUsersProvider(api, {
          val jsonProviders = o getJSONArray "providers"
          for (i <- 0 until jsonProviders.length) yield parseUsersProvider(api, jsonProviders.getJSONObject(i))
        }: _*) {
          index = o getInt "index"
        }
      case _ =>
        new CompositeUsersProvider(api)
    }

}