package ds13.insta.api.intrnl.model

import org.json.JSONObject
import ds13.logger.TraitJSON

class Comment(
  val createdAtUTC: Long,
  val contentType: String,
  val userId: Long,
  val bitFlags: Int,
  val createdAt: Long,
  val mediaId: Long,
  val text: String,
  val pk: Long,
  val ttype: Int,
  val user: User,
  val status: String)
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("created_at_utc", createdAtUTC)
      accumulate("content_type", contentType)
      accumulate("user_id", userId)
      accumulate("bit_flags", bitFlags)
      accumulate("created_at", createdAt)
      accumulate("media_id", mediaId)
      accumulate("text", text)
      accumulate("pk", pk)
      accumulate("type", ttype)
      user.toJSON foreach (t => accumulate("user", t))
      accumulate("status", status)
    })

}
