package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.intrnl.InternalAPI
import ds13.insta.api.intrnl.model.EmptyUsersList
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.api.intrnl.model.providers.TraitFilter
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.providers.UserListFilterProcessor
import ds13.insta.api.intrnl.model.providers.UserListFilterProcessor

object UserFollowingsUsersProvider {

  val NAME = "UserFollowingsUsersProvider"

}

class UserFollowingsUsersProvider(
  override val api: APIExecutor,
  val username: String)
    extends TraitUsersContainerWithMaxIdUsersProvider {

  override val name = UserFollowingsUsersProvider.NAME

  var userIdOpt: Option[Long] = None

  override def getNextContainer(iapi: InternalAPI, nextMaxIdOpt: Option[String])(implicit filter: Option[TraitFilter[User]]): TraitUsersContainerWithNextMaxId =
    api.internalFold(EmptyUsersList()) { iapi =>
      val uid = userIdOpt.getOrElse {
        val localUid = iapi.getUsernameInfo(username).id
        userIdOpt = Some(localUid)
        localUid
      }
      //new UserListFilterProcessor().process(iapi.getFollowers(uid, nextMaxIdOpt))
      iapi.getFollowers(uid, nextMaxIdOpt)
    }

  override def emptyContainer: TraitUsersContainerWithNextMaxId =
    EmptyUsersList()

  override def getJSONState: Option[JSONObject] =
    Some {
      val o = super.getJSONState.getOrElse(new JSONObject)
      o.accumulate("username", username)
      userIdOpt.foreach(userId => o.accumulate("user_id", userId))
      o
    }

}