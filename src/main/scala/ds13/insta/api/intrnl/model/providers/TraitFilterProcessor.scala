package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.Feed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId

trait TraitFilterProcessor[T <: TraitUsersContainerWithNextMaxId] {

  def process(t: T)(implicit filter: Option[TraitFilter[User]]): T =
    filter.fold(t)(f => createFiltered(t, f))

  def createFiltered(t: T, f: TraitFilter[User]): T

}