package ds13.insta.api.intrnl.model.providers

import ds13.insta.api.intrnl.model.User
import ds13.insta.impl.rc1.APIExecutor
import ds13.logger.TraitJSON
import org.json.JSONObject

trait TraitFilter[T] {

  def skip(t: T): Boolean

}