package ds13.insta.api.intrnl.model

trait TraitUsersContainer extends TraitSizeSupports {

  def getUser(index: Int): User

}