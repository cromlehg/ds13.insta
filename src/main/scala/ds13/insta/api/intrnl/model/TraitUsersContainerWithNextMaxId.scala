package ds13.insta.api.intrnl.model

import ds13.logger.TraitJSON

trait TraitUsersContainerWithNextMaxId extends TraitUsersContainer with TraitNextMaxIdProvider with TraitJSON {

}