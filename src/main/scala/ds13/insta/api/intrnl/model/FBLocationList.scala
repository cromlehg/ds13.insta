package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.logger.TraitJSON

class FBLocationList(
  val hasMore: Option[Boolean],
  val items: Seq[FBLocation])
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      hasMore.foreach(t => accumulate("has_more", t))
      items.foreach(_.toJSON.foreach(u => append("items", u)))
    })

}