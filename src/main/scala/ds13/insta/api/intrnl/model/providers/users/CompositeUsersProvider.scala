package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.impl.rc1.APIExecutor

object CompositeUsersProvider {

  val NAME = "CompositeUsersProvider"

}

class CompositeUsersProvider(
  override val api: APIExecutor,
  override val providers: TraitUsersProvider*)
    extends TraitCompositeUsersProvider {

  override val name = CompositeUsersProvider.NAME

}