package ds13.insta.api.intrnl.model.providers.users

import ds13.insta.api.intrnl.model.User
import org.json.JSONObject
import ds13.insta.api.intrnl.model.providers.TraitFilter

trait TraitCompositeUsersProvider extends TraitUsersProvider {

  val providers: Seq[TraitUsersProvider]

  var index = 0

  override def hasNext()(implicit filter: Option[TraitFilter[User]]): Boolean = {
    while (index < providers.length && !providers(index).hasNext) index += 1
    index < providers.length && providers(index).hasNext
  }

  override def apply[T](f: User => T): T =
    providers(index) apply f

  override def getJSONState: Option[JSONObject] =
    if (providers.isEmpty) None else
      Some(new JSONObject {
        providers.foreach(t => append("providers", t.toJSON.get))
        accumulate("index", index)
      })
}