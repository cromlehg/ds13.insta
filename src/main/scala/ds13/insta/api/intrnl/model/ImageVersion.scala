package ds13.insta.api.intrnl.model

import ds13.logger.TraitJSON
import org.json.JSONObject

class ImageVersion(
  val width: Int,
  val height: Int,
  val ttype: Int,
  val url: String)
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("width", width)
      accumulate("height", height)
      accumulate("type", ttype)
      accumulate("url", url)
    })

}