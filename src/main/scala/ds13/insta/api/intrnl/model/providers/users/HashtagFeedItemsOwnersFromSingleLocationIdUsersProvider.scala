package ds13.insta.api.intrnl.model.providers.users

import org.json.JSONObject

import ds13.insta.api.intrnl.InternalAPI
import ds13.insta.api.intrnl.model.EmptyFeed
import ds13.insta.api.intrnl.model.TraitUsersContainerWithNextMaxId
import ds13.insta.impl.rc1.APIExecutor
import ds13.insta.api.intrnl.model.providers.TraitFilter
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.providers.FeedFilterProcessor

object HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider {

  val NAME = "HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider"

}

class HashtagFeedItemsOwnersFromSingleLocationIdUsersProvider(
  override val api: APIExecutor,
  val locationId: Long)
    extends TraitHashtagFeedItemsOwnersUsersProvider {

  override val name = HashtagFeedItemsOwnersUsersProvider.NAME

  override def getNextContainer(iapi: InternalAPI, nextMaxIdOpt: Option[String])(implicit filter: Option[TraitFilter[User]]): TraitUsersContainerWithNextMaxId =
    iapi.getLocationFeed(locationId, nextMaxIdOpt)
    //new FeedFilterProcessor().process(iapi.getLocationFeed(locationId, nextMaxIdOpt))

  override def getJSONState: Option[JSONObject] =
    Some(super.getJSONState.getOrElse(new JSONObject).accumulate("location_id", locationId))

}