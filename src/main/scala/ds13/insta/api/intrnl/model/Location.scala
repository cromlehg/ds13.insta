package ds13.insta.api.intrnl.model

import org.json.JSONObject

import ds13.logger.TraitJSON

class Location(
  val address: String,
  val lng: Double,
  val city: String,
  val facebookPlacesId: Long,
  val name: String,
  val pk: Long,
  val lat: Double,
  val externalSource: String)
    extends TraitJSON {

  override def toJSON: Option[JSONObject] =
    Some(new JSONObject {
      accumulate("address", address)
      accumulate("lng", lng)
      accumulate("city", city)
      accumulate("facebook_places_id", facebookPlacesId)
      accumulate("name", name)
      accumulate("pk", pk)
      accumulate("lat", lat)
      accumulate("external_source", externalSource)
    })

}