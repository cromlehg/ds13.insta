package ds13.insta.api.intrnl

import java.io.StringWriter

import scala.collection.JavaConversions.asScalaBuffer

import org.apache.commons.codec.binary.Hex
import org.apache.http.HttpStatus
import org.json.JSONObject

import ds13.http.HTTPClientConsts
import ds13.insta.api.CommonAPI
import ds13.insta.storage.TraitAPIStorage
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec
import ds13.insta.api.intrnl.model.User
import ds13.insta.api.intrnl.model.FollowOperationInfo
import ds13.insta.api.intrnl.model.UsersList
import ds13.insta.api.intrnl.model.JSONParser
import ds13.insta.api.intrnl.model.UsersList
import ds13.insta.api.intrnl.model.Feed
import ds13.insta.api.CheckpointLoggedOut
import ds13.insta.api.CheckpointLoggedOutErrorGetVerifyPage
import ds13.insta.api.CheckpointLoggedOutErrorGetToken
import ds13.insta.api.CheckpointLoggedOutErrorGetSecondVerifyPage
import ds13.insta.api.CheckpointLoggedOut
import ds13.insta.api.CheckpointLoggedOutErrorGetThridVerifyPage
import ds13.insta.api.CheckpointLoggedOutErrorWrongCode
import ds13.insta.api.BadGatewayException
import ds13.insta.api.intrnl.model.FBLocationList

class InternalAPI(apiStorage: TraitAPIStorage)
    extends CommonAPI(apiStorage) {

  val URL = "https://instagram.com"

  val INSTAGRAM_INTERNAL_API_V1 = URL + "/api/v1/"

  val url_LOGIN = INSTAGRAM_INTERNAL_API_V1 + "accounts/login/"

  val FOLLOWINGS = "/following/"

  val FOLLOWERS = "/followers/"

  val LIKE = "/like/"

  val UNLIKE = "/unlike/"

  val INFO = "/info/"

  val INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS = INSTAGRAM_INTERNAL_API_V1 + "friendships/"

  val INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS_CREATE = INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS + "create/"

  val INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS_DESTROY = INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS + "destroy/"

  val INSTAGRAM_INTERNAL_API_V1_MEDIA = INSTAGRAM_INTERNAL_API_V1 + "media/"

  val INSTAGRAM_INTERNAL_API_V1_FEED = INSTAGRAM_INTERNAL_API_V1 + "feed/"

  val INSTAGRAM_INTERNAL_API_V1_FEED_USER = INSTAGRAM_INTERNAL_API_V1_FEED + "user/"

  val INSTAGRAM_INTERNAL_API_V1_FEED_HASHTAG = INSTAGRAM_INTERNAL_API_V1_FEED + "tag/"

  val INSTAGRAM_INTERNAL_API_V1_FEED_LOCATION = INSTAGRAM_INTERNAL_API_V1_FEED + "location/"

  val INSTAGRAM_INTERNAL_API_V1_USERS = INSTAGRAM_INTERNAL_API_V1 + "users/"

  val INSTAGRAM_INTERNAL_API_V1_FBSEARCH = INSTAGRAM_INTERNAL_API_V1 + "fbsearch/"

  val INSTAGRAM_INTERNAL_API_V1_FBSEARCH_PLACES = INSTAGRAM_INTERNAL_API_V1_FBSEARCH + "places/"

  val key = "b4a23f5e39b5929e0666ac5de94c89d1618a2916"

  val sha256_HMAC: Mac = Mac.getInstance("HmacSHA256")

  sha256_HMAC.init(new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256"))

  debug("Setup default common headers.")
  addCommonHeaders(
    "Connection" -> "Keep-Alive",
    "Accept-Language" -> "en-US",
    "Accept" -> "*/*",
    "User-Agent" -> ("Instagram " + apiStorage.getAppVersion + " " + apiStorage.getPlatform))

  val parser = new JSONParser

  val csrfmiddlewaretokenPattern = """\s+name\s*=\s*"csrfmiddlewaretoken"\s+value=\s*"(.*?)"\s*\/>""".r

  val checkpointOK = """<input\s+type\s*=\s*"submit"\s+name\s*=\s*"OK"\s+class\s*=\s*"checkpoint-button-neutral"\s+value\s*=\s*"OK"\s*>""".r

  var sessionId: String = null

  var csrftoken: String = null

  var userOpt: Option[User] = None

  var verCsrfmiddlewaretoken: String = null

  var verEx: CheckpointLoggedOut = null

  def loggedIn[T](f: User => T): T =
    f(userOpt.getOrElse {
      val user = login
      userOpt = Some(user)
      user
    })

  def passSMSCheck(code: Long) {
    debug("Try to prepare sending SMS verification code!")
    post(verEx.checkpointUrl).withFormArgs(
      "csrfmiddlewaretoken" -> verCsrfmiddlewaretoken,
      "response_code" -> code.toString).execute { implicit response =>
        debug("Send SMS verification code request prepared!")
        getStringContent(response) match {
          case Some(content) =>
            debug("OK, check response!")
            csrfmiddlewaretokenPattern.findFirstIn(content) match {
              case Some(of) => debug("It can be Ok") // try to find - OK
              case _        => throw new CheckpointLoggedOutErrorWrongCode(verEx.response, verEx.json, code)
            }
          case None => throw new CheckpointLoggedOutErrorGetThridVerifyPage(verEx.response, verEx.json)
        }
      }
  }

  def isNeedLoginCheckpointPass: Boolean =
    try loggedIn(_ => false) catch {
      case e: CheckpointLoggedOut =>
        debug("Checkpoint needs: " + e.checkpointUrl)
        getCookieValue("csrftoken").foreach(csrftoken = _)
        get(e.checkpointUrl).execute { implicit response =>
          getStringContent(response) match {
            case Some(content) =>
              debug("Try to get token...")
              csrfmiddlewaretokenPattern.findFirstMatchIn(content) match {
                case Some(csrfmiddlewaretokenM) =>
                  val csrfmiddlewaretoken = csrfmiddlewaretokenM.group(1)
                  debug("Token is: " + csrfmiddlewaretoken)
                  debug("Try to get send verification request by SMS.")
                  post(e.checkpointUrl).withFormArgs(
                    "csrfmiddlewaretoken" -> csrfmiddlewaretoken,
                    // or "Подтвердить SMS" for RU encoding                              
                    "sms" -> "Verify by SMS").execute { implicit response =>
                      debug("Getting content for SMS verification page...")
                      getStringContent(response) match {
                        case Some(content) =>
                          println(content)
                          debug("OK, now we waiting for verification code...")
                          verEx = e
                          true
                        case None => throw new CheckpointLoggedOutErrorGetSecondVerifyPage(e.response, e.json)
                      }
                    }
                case _ => throw new CheckpointLoggedOutErrorGetToken(e.response, e.json)
              }
            case _ => throw new CheckpointLoggedOutErrorGetVerifyPage(e.response, e.json)
          }
        }
      case e: Throwable => throw e
    }

  private def login: User =
    infTryAfterException(userOpt.getOrElse {
      debug("Start login")
      post(URL + "/api/v1/accounts/login/")
        .withFormArgs(
          "device_id" -> ("android-" + apiStorage.getDeviceId.get),
          "guid" -> apiStorage.getGUID.get,
          "username" -> apiStorage.accountStorage.getLogin,
          "password" -> apiStorage.accountStorage.getPass)
        .withMod(signMod)
        .execute(implicit response => okJSONInst { json =>
          val user = parser.parseUserContainedLoggedInUser(json)
          getCookieValue("csrftoken").foreach(csrftoken = _)
          getCookieValue("sessionid").foreach(sessionId = _)
          debug("Internal API login - OK!")
          user
        })
    })

  private def infTryAfterExceptionLoggedIn[T](f: User => T): T =
    loggedIn(user => infTryAfterException(f(user)))

  def getSelfFollowers(maxIdOpt: Option[String]): UsersList = infTryAfterExceptionLoggedIn { user =>
    getFollowers(user.id, maxIdOpt)
  }

  // Requested users follow to user with userId 
  def getFollowers(userId: Long, maxIdOpt: Option[String]): UsersList = infTryAfterExceptionLoggedIn { user =>
    debug("Get followers from user " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS + userId + FOLLOWERS)
      .withMod(t => mod(userId, t: _*))
      .withArgs(maxIdOpt.fold[Seq[(String, String)]](Seq.empty)(t => Seq("max_id" -> t)): _*)
      .execute(implicit response => okJSONInst(parser.parseUsersList))
  }

  def getSelfFollowings(maxIdOpt: Option[String]): UsersList = infTryAfterExceptionLoggedIn { user =>
    getFollowings(user.id, maxIdOpt)
  }

  // User with userId follow to requested users
  def getFollowings(userId: Long, maxIdOpt: Option[String]) = infTryAfterExceptionLoggedIn { user =>
    debug("Get followings from user " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS + userId + FOLLOWINGS)
      .withMod(t => mod(userId, t: _*))
      .withArgs(maxIdOpt.fold[Seq[(String, String)]](Seq.empty)(t => Seq("max_id" -> t)): _*)
      .execute(implicit response => okJSONInst(parser.parseUsersList))
  }

  def unfollow(userId: Long): FollowOperationInfo = infTryAfterExceptionLoggedIn { user =>
    debug("Try to unfollow from user with id " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS_DESTROY + userId + "/")
      .withMod(t => mod(userId, t: _*))
      .execute(implicit response => okJSONInst(parser.parseFriendshipStatus))
  }

  def follow(userId: Long): FollowOperationInfo = infTryAfterExceptionLoggedIn { user =>
    debug("Try to follow from user with id " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_FRIENDSHIPS_CREATE + userId + "/")
      .withMod(t => mod(userId, t: _*))
      .execute(implicit response => okJSONInst(parser.parseFriendshipStatus))
  }

  // TODO: Fix with result
  def unlike(mediaId: Long): Boolean = infTryAfterExceptionLoggedIn { user =>
    debug("Try to unlike media with id " + mediaId)
    post(INSTAGRAM_INTERNAL_API_V1_MEDIA + mediaId + UNLIKE)
      .withMod(t => mod(user.id, t: _*))
      .execute(implicit response => okJSONInst(_ => true))
  }

  // TODO: Fix with result
  def like(mediaId: Long): Boolean = infTryAfterExceptionLoggedIn { user =>
    debug("Try to like media with id " + mediaId)
    post(INSTAGRAM_INTERNAL_API_V1_MEDIA + mediaId + LIKE)
      .withMod(t => mod(user.id, t: _*))
      .execute(implicit response => okJSONInst(_ => true))
  }

  // FIXME: Not tested. Not implemented fully!
  // TODO:....
  def mediaInfo(mediaId: Long): Boolean = infTryAfterExceptionLoggedIn { user =>
    debug("Try to get info for media with id " + mediaId)
    post(INSTAGRAM_INTERNAL_API_V1_MEDIA + mediaId + INFO)
      .withMod(t => mod(user.id, t: _*))
      .execute(implicit response => okJSONInst(_ => true))
  }

  def getSelfUserFeed = infTryAfterExceptionLoggedIn { user => getUserFeed(user.id) }

  def getUserFeed(userId: Long): Feed = infTryAfterExceptionLoggedIn { user =>
    debug("Get feed for user with id " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_FEED_USER + userId + "/")
      .withMod(t => mod(userId, t: _*))
      .execute(implicit response => okJSONInst(parser.parseFeed))
  }

  def getHashtagFeeds(hashtag: String, maxIdOpt: Option[String]): Feed = infTryAfterExceptionLoggedIn { user =>
    debug("Get feed for hashtag " + hashtag)
    post(INSTAGRAM_INTERNAL_API_V1_FEED_HASHTAG + hashtag + "/")
      .withMod(t => mod(user.id, t: _*))
      .execute(implicit response => okJSONInst(parser.parseFeed))
  }

  def getSelfUserInfo: User = infTryAfterExceptionLoggedIn { user => getUserInfo(user.id) }

  def getUserInfo(userId: Long): User = infTryAfterExceptionLoggedIn { user =>
    debug("Get info for user id " + userId)
    post(INSTAGRAM_INTERNAL_API_V1_USERS + userId + "/info/")
      .withMod(t => mod(userId, t: _*))
      .execute(implicit response => okJSONInst(parser.parseUserContainedUser))
  }

  def getUsernameInfo(username: String): User = infTryAfterExceptionLoggedIn { user =>
    debug("Get info for user " + username)
    post(INSTAGRAM_INTERNAL_API_V1_USERS + username + "/usernameinfo/")
      .withMod(t => mod(user.id, t: _*))
      .execute(implicit response => okJSONInst(parser.parseUserContainedUser))
  }

  def searchFacebookLocation(query: String, maxIdOpt: Option[String]): FBLocationList = infTryAfterExceptionLoggedIn { user =>
    debug("Get Facebook places by query: \"" + query + "\"")
    post(INSTAGRAM_INTERNAL_API_V1_FBSEARCH_PLACES)
      .withMod(t => mod(user.id, t: _*))
      .withArgs(Seq("query" -> query) ++ maxIdOpt.fold[Seq[(String, String)]](Seq.empty)(t => Seq("max_id" -> t)): _*)
      .execute(implicit response => okJSONInst(parser.parseFBLocationList))
  }

  //   public function searchFBLocation($query)
  //    {
  //        $query = urlencode($query);
  //        return $this->request('fbsearch/places/')
  //        ->addParams('rank_token', $this->rank_token)
  //        ->addParams('query', $query)
  //        ->getResponse(new FBLocationResponse());
  //    }
  def getLocationFeed(locationId: Long, maxIdOpt: Option[String]): Feed = infTryAfterExceptionLoggedIn { user =>
    debug("Get feed for location with id: " + locationId)
    post(INSTAGRAM_INTERNAL_API_V1_FEED_LOCATION + locationId + "/")
      .withMod(t => mod(user.id, t: _*))
      .withArgs(maxIdOpt.fold[Seq[(String, String)]](Seq.empty)(t => Seq("max_id" -> t)): _*)
      .execute(implicit response => okJSONInst(parser.parseFeed))
  }

  //  
  //  def searchLocation(latitude: Double, longitude: Double, query: Option[String]) = {
  //    
  //  }

  //  def     public function searchLocation($latitude, $longitude, $query = null)
  //    {
  //        $locations = $this->request('location_search/')
  //        ->addParams('rank_token', $this->rank_token)
  //        ->addParams('latitude', $latitude)
  //        ->addParams('longitude', $longitude);
  //        if (!is_null($query)) {
  //            $locations->addParams('timestamp', time());
  //        } else {
  //            $locations->addParams('search_query', $query);
  //        }
  //        return $locations->getResponse(new LocationResponse());
  //    }

  // SHOULD IMPLEMENT
  /*
   
   // ???? May be get user last media?
      public function getUserFeed($usernameId, $maxid = null, $minTimestamp = null)
    {
        return $this->request("feed/user/$usernameId/")
        ->addParams('rank_token', $this->rank_token)
        ->addParams('ranked_content', 'true')
        ->addParams('max_id', (!is_null($maxid) ? $maxid : ''))
        ->addParams('min_timestamp', (!is_null($minTimestamp) ? $minTimestamp : ''))
        ->getResponse(new UserFeedResponse());
    }
    
    
    
      public function comment($mediaId, $commentText)
    {
        return $this->request("media/$mediaId/comment/")
        ->addPost('_uuid', $this->uuid)
        ->addPost('_uid', $this->username_id)
        ->addPost('_csrftoken', $this->token)
        ->addPost('comment_text', $commentText)
        ->getResponse(new CommentResponse());
    }
    
    
    public function deleteMedia($mediaId)
    {
        return $this->request("media/$mediaId/delete/")
        ->addPost('_uuid', $this->uuid)
        ->addPost('_uid', $this->username_id)
        ->addPost('_csrftoken', $this->token)
        ->addPost('media_id', $mediaId)
        ->getResponse(new MediaDeleteResponse());
    }

      public function deleteComment($mediaId, $commentId)
    {
        return $this->request("media/$mediaId/comment/$commentId/delete/")
        ->addPost('_uuid', $this->uuid)
        ->addPost('_uid', $this->username_id)
        ->addPost('_csrftoken', $this->token)
        ->getResponse(new DeleteCommentResponse());
    }*/
  /*
  private def parseFollowersFollowingsJSON(jsonObject: JSONObject): Option[UsersList] = {
    Some(new UsersList(
      if (jsonObject.has("next_max_id")) Some(jsonObject.getLong("next_max_id")) else None,
      parser.parseUsers(jsonObject getJSONArray "users"),
      jsonObject.getBoolean("big_list"),
      jsonObject.getInt("page_size")))
  }
*/
  private def mod(targetUserId: Long, args: (String, String)*): Seq[(String, String)] =
    signMod(
      "_uid" -> getCookieValue("ds_user_id").get,
      "_csrftoken" -> getCookieValue("csrftoken").get,
      "user_id" -> targetUserId.toString)

  private def toJSONString(args: (String, String)*): String = {
    val obj = new JSONObject
    args.foreach(entry => obj.put(entry._1, entry._2))
    val out = new StringWriter
    obj.write(out)
    val string = out.toString
    out.close
    string
  }

  private def sign(string: String): String =
    Hex.encodeHexString(sha256_HMAC.doFinal(string.getBytes("UTF-8")))

  private def signMod(args: (String, String)*): Seq[(String, String)] = {
    val jsonDataString = toJSONString(args: _*)
    Seq(
      "signed_body" -> (sign(jsonDataString) + "." + jsonDataString),
      "ig_sig_key_version" -> "4")
  }

}

object InternalAPI5Consts {

  val NAME = "Internal"

  val VERSION = "5"

}
