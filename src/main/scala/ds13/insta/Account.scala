package ds13.insta

class Account(val id: Long, username: Option[String], followersCount: Option[Int], followedByCount: Option[Int])