CREATE TABLE IF NOT EXISTS users (
  id                        SERIAL PRIMARY KEY,
  login		                VARCHAR(100) NOT NULL UNIQUE
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE proxies (
  id                        SERIAL PRIMARY KEY,
  ip						VARCHAR(16) NOT NULL,
  port						INT UNSIGNED,
  login						VARCHAR(100),
  pass						VARCHAR(100)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE accounts (
  id                        SERIAL PRIMARY KEY,
  owner_id                  BIGINT UNSIGNED NOT NULL,
  login		                VARCHAR(100) NOT NULL UNIQUE,
  pass		                VARCHAR(100) NOT NULL UNIQUE,
  account_id                BIGINT UNSIGNED UNIQUE,
  proxy_id					BIGINT UNSIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE api (
  id						SERIAL PRIMARY KEY,
  account_id				BIGINT UNSIGNED NOT NULL,
  name						VARCHAR(100) NOT NULL,
  version					VARCHAR(20) NOT NULL,
  device_id					VARCHAR(100) UNIQUE,
  guid						VARCHAR(100) UNIQUE,
  proxy_id					BIGINT UNSIGNED
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_not_unfollow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_not_to_follow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_ever_followed (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE lists_to_follow (
  account_id                BIGINT UNSIGNED NOT NULL,
  user_id                   BIGINT UNSIGNED NOT NULL,
  PRIMARY KEY (account_id, user_id)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE launches (
  id                        SERIAL PRIMARY KEY,
  bot_name	                VARCHAR(100) NOT NULL,
  api_id	                BIGINT UNSIGNED NOT NULL,
  state                     INT NOT NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
