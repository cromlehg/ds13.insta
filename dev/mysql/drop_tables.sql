DROP TABLE IF EXISTS launches;

DROP TABLE IF EXISTS lists_not_unfollow;

DROP TABLE IF EXISTS lists_not_to_follow;

DROP TABLE IF EXISTS lists_ever_followed;

DROP TABLE IF EXISTS lists_to_follow;

DROP TABLE IF EXISTS api;

DROP TABLE IF EXISTS accounts;

DROP TABLE IF EXISTS proxies;

DROP TABLE IF EXISTS users;

