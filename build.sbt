name := "insta"

organization := "ds13"

version := "0.1"

scalaVersion := "2.11.8"

resolvers += "DS13" at "http://maven.siamway.ru/"

libraryDependencies ++= Seq(
  "ds13" % "logger_2.11" % "0.1" changing(),
  "ds13" % "http_2.11" % "0.1" changing(),
  "ds13" % "bots_2.11" % "0.1" changing(),
  "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test",
  "org.scala-lang" % "scala-reflect" % "2.11.8",
  "mysql" % "mysql-connector-java" % "6.0.5",
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "com.typesafe.slick" % "slick-hikaricp_2.11" % "3.1.1",
  "org.slf4j" % "slf4j-nop" % "1.6.4"
)

isSnapshot := true

val resolver = Resolver.ssh("DS13_Publish", "maven.siamway.ru", "/var/www/vhosts/maven.siamway.ru/httpdocs") withPermissions ("0644")

publishTo := Some(resolver as ("maven"))

publishArtifact in (packageSrc) := true

publishArtifact in (packageDoc) := true

